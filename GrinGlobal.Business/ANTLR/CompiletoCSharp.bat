java -cp antlr-4.5-complete.jar org.antlr.v4.Tool SeQueryLexer.g4 -visitor -Dlanguage=CSharp
java -cp antlr-4.5-complete.jar org.antlr.v4.Tool SeQueryParser.g4 -visitor -Dlanguage=CSharp

PAUSE

REM For Testing under Windows Java
REM java -cp antlr-4.5-complete.jar org.antlr.v4.Tool SeQueryLexer.g4
REM java -cp antlr-4.5-complete.jar org.antlr.v4.Tool SeQueryParser.g4
REM javac -cp antlr-4.5-complete.jar SeQuery*.java
REM java -cp .;antlr-4.5-complete.jar org.antlr.v4.runtime.misc.TestRig SeQuery mixedQuery -gui
REM Enter query then control-Z and enter. Will display a parse tree diagram.