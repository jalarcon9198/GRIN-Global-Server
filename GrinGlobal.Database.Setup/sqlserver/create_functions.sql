SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_a_is_available]')
	AND type in (N'FN', N'IF',N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fn_a_is_available]
GO

CREATE FUNCTION [dbo].[fn_a_is_available](@id int) RETURNS char AS
BEGIN
	DECLARE @avail char(1)

	SET @avail = CASE WHEN EXISTS
		(SELECT 1 FROM inventory WHERE accession_id = @id AND is_available = 'Y' and is_distributable = 'Y')
		THEN 'Y' ELSE 'N' END

	RETURN @avail
END
GO

