BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

/* ----- Inventory Viability Data ----- */

/********* Drop FK constraints to the inventory_viability_data table ***********/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_ivd_c]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability_data]'))
ALTER TABLE [dbo].[inventory_viability_data] DROP CONSTRAINT [fk_ivd_c]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_ivd_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability_data]'))
ALTER TABLE [dbo].[inventory_viability_data] DROP CONSTRAINT [fk_ivd_created]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_ivd_iv]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability_data]'))
ALTER TABLE [dbo].[inventory_viability_data] DROP CONSTRAINT [fk_ivd_iv]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_ivd_modified]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability_data]'))
ALTER TABLE [dbo].[inventory_viability_data] DROP CONSTRAINT [fk_ivd_modified]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_ivd_ori]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability_data]'))
ALTER TABLE [dbo].[inventory_viability_data] DROP CONSTRAINT [fk_ivd_ori]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_ivd_owned]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability_data]'))
ALTER TABLE [dbo].[inventory_viability_data] DROP CONSTRAINT [fk_ivd_owned]
GO



/****************** Make a copy of the inventory_viability_data table's data ********************/
SELECT * INTO inventory_viability_data_temp FROM inventory_viability_data



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[inventory_viability_data]') AND type in (N'U'))
DROP TABLE [dbo].[inventory_viability_data]
GO


/****** Object:  Table [dbo].[inventory_viability_data] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[inventory_viability_data](
	[inventory_viability_data_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_viability_id] [int] NOT NULL,
	[order_request_item_id] [int] NULL,
	[counter_cooperator_id] [int] NULL,
	[replication_number] [int] NOT NULL,
	[count_number] [int] NOT NULL,
	[count_date] [datetime2](7) NOT NULL,
	[normal_count] [int] NOT NULL,
	[abnormal_count] [int] NULL,
	[dormant_count] [int] NULL,
	[hard_count] [int] NULL,
	[empty_count] [int] NULL,
	[infested_count] [int] NULL,
	[dead_count] [int] NULL,
	[unknown_count] [int] NULL,
	[estimated_dormant_count] [int] NULL,
	[treated_dormant_count] [int] NULL,
	[confirmed_dormant_count] [int] NULL,
	[replication_count] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_inventory_viability_data] PRIMARY KEY CLUSTERED 
(
	[inventory_viability_data_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/********** Load the data back into the inventory_viability_data table *********/
SET IDENTITY_INSERT inventory_viability_data ON 
GO
INSERT INTO inventory_viability_data
	(
	inventory_viability_data_id,
	inventory_viability_id,
	order_request_item_id,
	counter_cooperator_id,
	replication_number,
	count_number,
	count_date,
	normal_count,
	abnormal_count,
	dormant_count,
	hard_count,
	empty_count,
	infested_count,
	dead_count,
	unknown_count,
	estimated_dormant_count,
	treated_dormant_count,
	confirmed_dormant_count,
	replication_count,
	note,
	created_date,
	created_by,
	modified_date,
	modified_by,
	owned_date,
	owned_by
	)
SELECT 
	inventory_viability_data_id,
	inventory_viability_id,
	order_request_item_id,
	counter_cooperator_id,
	replication_number,
	count_number,
	count_date,
	normal_count,
	abnormal_count,
	dormant_count,
	NULL,
	empty_count,
	infested_count,
	dead_count,
	unknown_count,
	estimated_dormant_count,
	treated_dormant_count,
	confirmed_dormant_count,
	replication_count,
	note,
	created_date,
	created_by,
	modified_date,
	modified_by,
	owned_date,
	owned_by
FROM inventory_viability_data_temp
GO
SET IDENTITY_INSERT inventory_viability_data OFF
GO


/********* RE-Add FK constraints to the inventory_viability_data table ***********/
ALTER TABLE [dbo].[inventory_viability_data]  WITH CHECK ADD  CONSTRAINT [fk_ivd_c] FOREIGN KEY([counter_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[inventory_viability_data] CHECK CONSTRAINT [fk_ivd_c]
GO

ALTER TABLE [dbo].[inventory_viability_data]  WITH CHECK ADD  CONSTRAINT [fk_ivd_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[inventory_viability_data] CHECK CONSTRAINT [fk_ivd_created]
GO

ALTER TABLE [dbo].[inventory_viability_data]  WITH CHECK ADD  CONSTRAINT [fk_ivd_iv] FOREIGN KEY([inventory_viability_id])
REFERENCES [dbo].[inventory_viability] ([inventory_viability_id])
GO

ALTER TABLE [dbo].[inventory_viability_data] CHECK CONSTRAINT [fk_ivd_iv]
GO

ALTER TABLE [dbo].[inventory_viability_data]  WITH CHECK ADD  CONSTRAINT [fk_ivd_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[inventory_viability_data] CHECK CONSTRAINT [fk_ivd_modified]
GO

ALTER TABLE [dbo].[inventory_viability_data]  WITH CHECK ADD  CONSTRAINT [fk_ivd_ori] FOREIGN KEY([order_request_item_id])
REFERENCES [dbo].[order_request_item] ([order_request_item_id])
GO

ALTER TABLE [dbo].[inventory_viability_data] CHECK CONSTRAINT [fk_ivd_ori]
GO

ALTER TABLE [dbo].[inventory_viability_data]  WITH CHECK ADD  CONSTRAINT [fk_ivd_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[inventory_viability_data] CHECK CONSTRAINT [fk_ivd_owned]
GO




/********* RE-Add FK constraints in other tables ***********/
-- None --


/********* RE-Add indexes to the inventory_viability_data table ***********/
CREATE NONCLUSTERED INDEX [ndx_fk_ivd_c] ON [dbo].[inventory_viability_data] 
(
	[counter_cooperator_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_ivd_created] ON [dbo].[inventory_viability_data] 
(
	[created_by] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_ivd_iv] ON [dbo].[inventory_viability_data] 
(
	[inventory_viability_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_ivd_modified] ON [dbo].[inventory_viability_data] 
(
	[modified_by] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_ivd_ori] ON [dbo].[inventory_viability_data] 
(
	[order_request_item_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_ivd_owned] ON [dbo].[inventory_viability_data] 
(
	[owned_by] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ivd] ON [dbo].[inventory_viability_data] 
(
	[inventory_viability_id] ASC,
	[replication_number] ASC,
	[count_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

drop table inventory_viability_data_temp
GO
COMMIT
