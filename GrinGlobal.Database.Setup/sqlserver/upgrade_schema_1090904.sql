BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

/* ----- Inventory Viability Rule ----- */

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_ivr_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability_rule]'))
ALTER TABLE [dbo].[inventory_viability_rule] DROP CONSTRAINT [fk_ivr_created]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_ivr_modified]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability_rule]'))
ALTER TABLE [dbo].[inventory_viability_rule] DROP CONSTRAINT [fk_ivr_modified]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_ivr_owned]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability_rule]'))
ALTER TABLE [dbo].[inventory_viability_rule] DROP CONSTRAINT [fk_ivr_owned]
GO


IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[inventory_viability_rule]') AND name = N'ndx_fk_ivr_created')
DROP INDEX [ndx_fk_ivr_created] ON [dbo].[inventory_viability_rule] WITH ( ONLINE = OFF )
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[inventory_viability_rule]') AND name = N'ndx_fk_ivr_modified')
DROP INDEX [ndx_fk_ivr_modified] ON [dbo].[inventory_viability_rule] WITH ( ONLINE = OFF )
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[inventory_viability_rule]') AND name = N'ndx_fk_ivr_owned')
DROP INDEX [ndx_fk_ivr_owned] ON [dbo].[inventory_viability_rule] WITH ( ONLINE = OFF )
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[inventory_viability_rule]') AND name = N'ndx_uniq_ivr')
DROP INDEX [ndx_uniq_ivr] ON [dbo].[inventory_viability_rule] WITH ( ONLINE = OFF )
GO

/****************** Make a copy of the inventory_viability_rule table's data ********************/
SELECT * INTO inventory_viability_rule_temp FROM inventory_viability_rule




/********* Drop FK constraints in other tables ***********/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_iv_ivr]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability]'))
ALTER TABLE [dbo].[inventory_viability] DROP CONSTRAINT [fk_iv_ivr]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_ivrm_ivr]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability_rule_map]'))
ALTER TABLE [dbo].[inventory_viability_rule_map] DROP CONSTRAINT [fk_ivrm_ivr]
GO



/****** Object:  Table [dbo].[inventory_viability_rule]    Script Date: 03/13/2014 06:00:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[inventory_viability_rule]') AND type in (N'U'))
DROP TABLE [dbo].[inventory_viability_rule]
GO


/****** Object:  Table [dbo].[inventory_viability_rule]    Script Date: 03/13/2014 06:00:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[inventory_viability_rule](
	[inventory_viability_rule_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[substrata] [nvarchar](100) NULL,
	[seeds_per_replicate] [int] NULL,
	[number_of_replicates] [int] NULL,
	[requirements] [nvarchar](max) NULL,
	[category_code] [nvarchar](20) NULL,
	[temperature_range] [nvarchar](100) NULL,
	[count_regime_days] [nvarchar](100) NULL,
	[moisture] [nvarchar](100) NULL,
	[prechill] [nvarchar](100) NULL,
	[lighting] [nvarchar](100) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_inventory_viability_rule] PRIMARY KEY CLUSTERED 
(
	[inventory_viability_rule_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/********** Load the data back into the inventory_viability_rule table *********/
SET IDENTITY_INSERT inventory_viability_rule ON 
GO
INSERT INTO inventory_viability_rule
	(
	inventory_viability_rule_id,
	name,
	substrata,
	seeds_per_replicate,
	number_of_replicates,
	requirements,
	category_code,
	temperature_range,
	count_regime_days,
	moisture,
	prechill,
	lighting,
	note,
	created_date,
	created_by,
	modified_date,
	modified_by,
	owned_date,
	owned_by
	)
SELECT 
	inventory_viability_rule_id,
	name,
	substrata,
	seeds_per_replicate,
	number_of_replicates,
	requirements,
	category_code,
	temperature_range,
	count_regime_days,
	moisture,
	prechill,
	lighting,
	note,
	created_date,
	created_by,
	modified_date,
	modified_by,
	owned_date,
	owned_by
FROM inventory_viability_rule_temp
GO
SET IDENTITY_INSERT inventory_viability_rule OFF
GO



/********* RE-Add FK constraints to inventory_viability_rule table ***********/
ALTER TABLE [dbo].[inventory_viability_rule]  WITH CHECK ADD  CONSTRAINT [fk_ivr_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[inventory_viability_rule] CHECK CONSTRAINT [fk_ivr_created]
GO

ALTER TABLE [dbo].[inventory_viability_rule]  WITH CHECK ADD  CONSTRAINT [fk_ivr_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[inventory_viability_rule] CHECK CONSTRAINT [fk_ivr_modified]
GO

ALTER TABLE [dbo].[inventory_viability_rule]  WITH CHECK ADD  CONSTRAINT [fk_ivr_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[inventory_viability_rule] CHECK CONSTRAINT [fk_ivr_owned]
GO



/********* RE-Add FK constraints in other tables ***********/
ALTER TABLE [dbo].[inventory_viability]  WITH CHECK ADD  CONSTRAINT [fk_iv_ivr] FOREIGN KEY([inventory_viability_rule_id])
REFERENCES [dbo].[inventory_viability_rule] ([inventory_viability_rule_id])
GO

ALTER TABLE [dbo].[inventory_viability] CHECK CONSTRAINT [fk_iv_ivr]
GO

ALTER TABLE [dbo].[inventory_viability_rule_map]  WITH CHECK ADD  CONSTRAINT [fk_ivrm_ivr] FOREIGN KEY([inventory_viability_rule_id])
REFERENCES [dbo].[inventory_viability_rule] ([inventory_viability_rule_id])
GO

ALTER TABLE [dbo].[inventory_viability_rule_map] CHECK CONSTRAINT [fk_ivrm_ivr]
GO



/****** Object:  Index [ndx_fk_ivr_created]    Script Date: 03/13/2014 06:02:20 ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivr_created] ON [dbo].[inventory_viability_rule] 
(
	[created_by] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_ivr_modified]    Script Date: 03/13/2014 06:02:42 ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivr_modified] ON [dbo].[inventory_viability_rule] 
(
	[modified_by] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_ivr_owned]    Script Date: 03/13/2014 06:02:51 ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivr_owned] ON [dbo].[inventory_viability_rule] 
(
	[owned_by] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_uniq_ivr]    Script Date: 03/13/2014 06:03:17 ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ivr] ON [dbo].[inventory_viability_rule] 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


/**************** Drop the inventory_viability_rule_temp table *****************/
DROP TABLE inventory_viability_rule_temp



/****** Object:  Index [ndx_fk_ori_wori]    Script Date: 5/10/2017 10:56:54 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ori_wori] ON [dbo].[order_request_item]
(
	[web_order_request_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT

