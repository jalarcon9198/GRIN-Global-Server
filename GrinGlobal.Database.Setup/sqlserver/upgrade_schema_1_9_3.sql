/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT


BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_inv_name ALTER COLUMN plant_name nvarchar(200) NOT NULL;
GO
UPDATE dbo.accession_inv_name SET is_web_visible = 'Y' WHERE is_web_visible IS NULL;
GO
ALTER TABLE dbo.accession_inv_name ALTER COLUMN is_web_visible nvarchar(1) NOT NULL;
GO
ALTER TABLE dbo.app_setting ALTER COLUMN value nvarchar(MAX) NULL;
GO
ALTER TABLE dbo.cooperator ALTER COLUMN organization nvarchar(100) NULL;
GO
COMMIT


BEGIN TRANSACTION
GO
ALTER TABLE dbo.inventory_viability_rule
	DROP CONSTRAINT fk_ivr_t
GO
ALTER TABLE dbo.taxonomy_species SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
DROP INDEX ndx_uniq_ivr ON dbo.inventory_viability_rule
GO
DROP INDEX ndx_fk_ivr_t ON dbo.inventory_viability_rule
GO
ALTER TABLE dbo.inventory_viability_rule
	DROP COLUMN taxonomy_species_id
GO
ALTER TABLE dbo.inventory_viability_rule SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.inventory_viability_rule.seeds', N'Tmp_seeds_per_replicate', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.inventory_viability_rule.replicates', N'Tmp_number_of_replicates_1', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.inventory_viability_rule.Tmp_seeds_per_replicate', N'seeds_per_replicate', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.inventory_viability_rule.Tmp_number_of_replicates_1', N'number_of_replicates', 'COLUMN' 
GO
ALTER TABLE dbo.inventory_viability_rule SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/****** Object:  Index [ndx_uniq_ivr]    Script Date: 3/19/2018 8:14:17 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ivr] ON [dbo].[inventory_viability_rule]
(
	[name] ASC,
	[substrata] ASC,
	[category_code] ASC,
	[temperature_range] ASC,
	[count_regime_days] ASC,
	[moisture] ASC,
	[prechill] ASC,
	[lighting] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

