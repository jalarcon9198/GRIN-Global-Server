/*
   Monday, March 19, 20183:51:00 PM
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION

ALTER TABLE order_request_item ALTER COLUMN name nvarchar(200) NULL;

ALTER TABLE web_order_request_item ALTER COLUMN name nvarchar(200) NULL;

ALTER TABLE cooperator ALTER COLUMN job nvarchar(100) NULL;
ALTER TABLE cooperator ALTER COLUMN	address_line1 nvarchar(100) NULL;
ALTER TABLE cooperator ALTER COLUMN	address_line2 nvarchar(100) NULL;
ALTER TABLE cooperator ALTER COLUMN	address_line3 nvarchar(100) NULL;
ALTER TABLE cooperator ALTER COLUMN	city nvarchar(100) NULL;
ALTER TABLE cooperator ALTER COLUMN	postal_index nvarchar(100) NULL;
ALTER TABLE cooperator ALTER COLUMN	secondary_organization nvarchar(100) NULL;
ALTER TABLE cooperator ALTER COLUMN	secondary_address_line1 nvarchar(100) NULL;
ALTER TABLE cooperator ALTER COLUMN	secondary_address_line2 nvarchar(100) NULL;
ALTER TABLE cooperator ALTER COLUMN	secondary_address_line3 nvarchar(100) NULL;
ALTER TABLE cooperator ALTER COLUMN	secondary_city nvarchar(100) NULL;
ALTER TABLE cooperator ALTER COLUMN	secondary_postal_index nvarchar(100) NULL;

EXEC sp_rename 'dbo.taxonomy_genus.is_hybrid', 'hybrid_code', 'COLUMN';
ALTER TABLE dbo.taxonomy_genus ALTER COLUMN hybrid_code nvarchar(20) NULL;
GO
UPDATE dbo.taxonomy_genus SET hybrid_code = CASE WHEN hybrid_code = 'Y' THEN 'X' ELSE null END

COMMIT
