/*
   Monday, March 19, 20183:51:00 PM
   User: 
   Server: ARSMDBE3445BLD2\SQLEXPRESS
   Database: gg196
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION

-- upgrade inventory action
GO
ALTER TABLE dbo.inventory_action
	DROP CONSTRAINT fk_ia_m
GO
ALTER TABLE dbo.method SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.method', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.method', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.method', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.inventory_action
	DROP CONSTRAINT fk_ia_i
GO
ALTER TABLE dbo.inventory SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.inventory', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.inventory', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.inventory', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.inventory_action
	DROP CONSTRAINT fk_ia_c
GO
ALTER TABLE dbo.inventory_action
	DROP CONSTRAINT fk_ia_created
GO
ALTER TABLE dbo.inventory_action
	DROP CONSTRAINT fk_ia_modified
GO
ALTER TABLE dbo.inventory_action
	DROP CONSTRAINT fk_ia_owned
GO
ALTER TABLE dbo.cooperator SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_inventory_action
	(
	inventory_action_id int NOT NULL IDENTITY (1, 1),
	inventory_id int NOT NULL,
	action_name_code nvarchar(20) NOT NULL,
	started_date datetime2(7) NULL,
	started_date_code nvarchar(20) NULL,
	completed_date datetime2(7) NULL,
	completed_date_code nvarchar(20) NULL,
	quantity decimal(18, 5) NULL,
	quantity_unit_code nvarchar(20) NULL,
	form_code nvarchar(20) NULL,
	cooperator_id int NULL,
	method_id int NULL,
	note nvarchar(MAX) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_inventory_action SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_inventory_action TO gg_user  AS dbo
GO
GRANT INSERT ON dbo.Tmp_inventory_action TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_inventory_action TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_inventory_action TO gg_search  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_inventory_action TO gg_user  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_inventory_action ON
GO
IF EXISTS(SELECT * FROM dbo.inventory_action)
	 EXEC('INSERT INTO dbo.Tmp_inventory_action (inventory_action_id, inventory_id, action_name_code, completed_date, completed_date_code, quantity, quantity_unit_code, form_code, cooperator_id, method_id, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT inventory_action_id, inventory_id, action_name_code, action_date, action_date_code, quantity, quantity_unit_code, form_code, cooperator_id, method_id, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.inventory_action WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_inventory_action OFF
GO
DROP TABLE dbo.inventory_action
GO
EXECUTE sp_rename N'dbo.Tmp_inventory_action', N'inventory_action', 'OBJECT' 
GO
ALTER TABLE dbo.inventory_action ADD CONSTRAINT
	PK_inventory_action PRIMARY KEY CLUSTERED 
	(
	inventory_action_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX ndx_fk_ia_c ON dbo.inventory_action
	(
	cooperator_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ia_created ON dbo.inventory_action
	(
	created_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ia_i ON dbo.inventory_action
	(
	inventory_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ia_m ON dbo.inventory_action
	(
	method_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ia_modified ON dbo.inventory_action
	(
	modified_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ia_owned ON dbo.inventory_action
	(
	owned_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_ia ON dbo.inventory_action
	(
	inventory_id,
	action_name_code,
	started_date,
	completed_date,
	form_code,
	quantity,
	method_id,
	cooperator_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.inventory_action ADD CONSTRAINT
	fk_ia_c FOREIGN KEY
	(
	cooperator_id
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.inventory_action ADD CONSTRAINT
	fk_ia_created FOREIGN KEY
	(
	created_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.inventory_action ADD CONSTRAINT
	fk_ia_i FOREIGN KEY
	(
	inventory_id
	) REFERENCES dbo.inventory
	(
	inventory_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.inventory_action ADD CONSTRAINT
	fk_ia_m FOREIGN KEY
	(
	method_id
	) REFERENCES dbo.method
	(
	method_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.inventory_action ADD CONSTRAINT
	fk_ia_modified FOREIGN KEY
	(
	modified_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.inventory_action ADD CONSTRAINT
	fk_ia_owned FOREIGN KEY
	(
	owned_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.inventory_action', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.inventory_action', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.inventory_action', 'Object', 'CONTROL') as Contr_Per 




/****** Object:  Index [ndx_uniq_cit]    Script Date: 3/19/2018 7:18:29 PM ******/
DROP INDEX [ndx_uniq_cit] ON [dbo].[citation]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [ndx_uniq_cit]    Script Date: 3/19/2018 7:18:29 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_cit] ON [dbo].[citation]
(
	[literature_id] ASC,
	[citation_title] ASC,
	[accession_id] ASC,
	[method_id] ASC,
	[taxonomy_species_id] ASC,
	[taxonomy_genus_id] ASC,
	[taxonomy_family_id] ASC,
	[accession_ipr_id] ASC,
	[accession_pedigree_id] ASC,
	[genetic_marker_id] ASC,
	[type_code] ASC,
	[unique_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO