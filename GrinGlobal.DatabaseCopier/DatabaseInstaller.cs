﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using Microsoft.Win32;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Windows.Forms;
using GrinGlobal.Core;
using GrinGlobal.Core.DataManagers;
using GrinGlobal.DatabaseInspector;
using System.Text;
using GrinGlobal.DatabaseInspector.PostgreSql;
using System.Data;
using GrinGlobal.InstallHelper;
using System.Net;

using System.Threading;
using System.Threading.Tasks;

namespace GrinGlobal.DatabaseCopier {
    [RunInstaller(true)]
    public partial class DatabaseInstaller : Installer {
        public DatabaseInstaller() {
            InitializeComponent();

        }

        //private void setSuperUserPassword(IDictionary savedState, string newPassword) {
        //    savedState["PASSWORD"] = Crypto.EncryptText(newPassword);
        //}

        private frmProgress _frmProgress;

        private const int SLEEP_TIME = 10;

        private object _threadSync = new object();
        private void waitForUserInput(BackgroundWorker worker, object state, int maxWaitMilliseconds) {

            // block current thread until user has responded to the input
            if (worker != null) {
                worker.ReportProgress(0, state);
                // THIS CAUSES ISSUES!  Some threading problem exists, do NOT remove timeout...
                // Toolkit.BlockThread();
                Toolkit.BlockThread(maxWaitMilliseconds);
            }



        }

        private bool _workerDone;
        private void progressDone() {
            try {
                _workerDone = true;
                if (_frmProgress != null) {
                    _frmProgress.Cursor = Cursors.Default;
                    _frmProgress.Close();
                    _frmProgress = null;
                }
            } finally {
                // make sure everything continues to process so we will eventually exit at some point
                Toolkit.UnblockBlockedThread();
            }
        }

        void c_OnProgress(object sender, ProgressEventArgs pea) {
            worker_ProgressChanged(sender, new ProgressChangedEventArgs(0, pea.Message)); 
        }


        //private bool promptForSuperUserPassword(string engine, string instanceName, ref string superUserPassword, ref bool windowsAuth) {

        //    superUserPassword = null;
        //    frmLogin login = new frmLogin();
        //    DialogResult dr = login.ShowDialog(engine, instanceName, windowsAuth);
        //    if (dr == DialogResult.OK) {
        //        superUserPassword = login.SuperUserPassword;
        //        windowsAuth = login.UseWindowsAuthentication;
        //        return true;
        //    } else {
        //        return false;
        //    }
        //    //}

        //}

        private DialogResult promptForDatabaseLoginInfo(BackgroundWorker worker, string engine, string instanceName, bool windowsAuth, string gguacPath, ref DatabaseEngineUtil dbEngineUtil, ref string superUserPassword, bool allowSkip) {
           
            if (worker != null) {

                var args = new WorkerArguments();
                args.Properties["prompt"] = "dbEngineUtil";
                args.Properties["engine"] = engine;
                args.Properties["instanceName"] = instanceName;
                args.Properties["windowsAuth"] = windowsAuth;
                args.Properties["gguacPath"] = gguacPath;
                args.Properties["dbEngineUtil"] = dbEngineUtil;
                args.Properties["superUserPassword"] = superUserPassword;
                args.Properties["dialogresult"] = null;
                args.Properties["allowSkip"] = allowSkip;
                waitForUserInput(worker, args, int.MaxValue);  // wait forever?

                dbEngineUtil = args.Properties["dbEngineUtil"] as DatabaseEngineUtil;
                superUserPassword = args.Properties["superUserPassword"] as string;

                var dr = (DialogResult)args.Properties["dialogresult"];
                return dr;

            } else {


                var f = new frmDatabaseLoginPrompt();
                var autoLogin = Utility.GetAutoLogin(Context, null);  // enable auto login only if it's a passive install
                //MessageBox.Show("autologin: " + enableAutoLogin);
                //throw new InvalidOperationException("unconditionally bombing out");
                var dr = f.ShowDialog(
                        Toolkit.GetWindowHandle("GRIN-Global Database"), 
                        dbEngineUtil, 
                        true,
                        true,
                        false, 
                        superUserPassword, 
                        dbEngineUtil == null ? null : (bool?)dbEngineUtil.UseWindowsAuthentication,
                        autoLogin,
                        allowSkip);
                if (dr == DialogResult.OK) {
                    superUserPassword = f.txtPassword.Text;
                    dbEngineUtil = f.DatabaseEngineUtil;
                }
                return dr;
            }
        }

        private DialogResult showMessageBox(BackgroundWorker worker, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon) {
            if (worker == null) {
                try {
                    return MessageBox.Show(_frmProgress, text, caption, buttons, icon);
                } catch {
                    // if the form is disposing, showing a messagebox with it as a owner will fail.
                    return MessageBox.Show(text, caption, buttons, icon);
                }
            } else {
                var args = new WorkerArguments();
                args.Properties["prompt"] = "messagebox";
                args.Properties["text"] = text;
                args.Properties["caption"] = caption;
                args.Properties["buttons"] = buttons;
                args.Properties["icon"] = icon;
                waitForUserInput(worker, args, int.MaxValue);  // wait forever?
                object result = null;
                if (args.Properties.TryGetValue("dialogresult", out result)) {
                    return (DialogResult)result;
                } else {
                    return DialogResult.OK;
                }
            }
        }


        private BackgroundWorker initWorker() {
            var worker = new BackgroundWorker();
            _workerDone = false;
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            return worker;
        }

        private void waitForWorkerToComplete(BackgroundWorker worker) {
            // do a cheesy spin wait here to make sure gui thread displays updates to user...
            while (worker.IsBusy && !_workerDone) {
                Thread.Sleep(SLEEP_TIME);
                Application.DoEvents();
            }
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            progressDone();
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            bool unblockThread = true;
            try {
                if (e.UserState is string) {
                    // background thread should not be blocked, so don't unblock it in the finally {}
                    unblockThread = false;
                    updateProgressForm(e.UserState as string, true);
                } else if (e.UserState is ApplicationException) {
                    var msg = Toolkit.Cut((e.UserState as ApplicationException).Message, 0, 7990);
                    EventLog.WriteEntry("GRIN-Global Database", msg, EventLogEntryType.Error);
                    updateProgressForm(msg, false);
                    showMessageBox(null, msg, getDisplayMember("ApplicationException", "Application Exception Occurred"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                } else if (e.UserState is Exception) {
                    var msg = Toolkit.Cut((e.UserState as Exception).Message, 0, 7990);
                    EventLog.WriteEntry("GRIN-Global Database", msg, EventLogEntryType.Error);
                    updateProgressForm(msg, false);
                    showMessageBox(null, msg, getDisplayMember("Exception", "Exception Occurred"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                } else if (e.UserState is WorkerArguments) {
                    var args = e.UserState as WorkerArguments;
                    var promptType = ("" + args.Properties["prompt"]).ToLower();
                    switch(promptType){
                        case "dbengineutil":
                            var engine = args.Properties["engine"] as string;
                            var instanceName = args.Properties["instanceName"] as string;
                            var windowsAuth = (bool)args.Properties["windowsAuth"];
                            var gguacPath = args.Properties["gguacPath"] as string;
                            var dbEngineUtil = args.Properties["dbEngineUtil"] as DatabaseEngineUtil;
                            var superUserPassword = args.Properties["superUserPassword"] as string;
                            var allowSkip = Toolkit.ToBoolean(args.Properties["allowSkip"], false);
                            args.Properties["dialogresult"] = promptForDatabaseLoginInfo(null, engine, instanceName, windowsAuth, gguacPath, ref dbEngineUtil, ref superUserPassword, allowSkip);
                            args.Properties["dbEngineUtil"] = dbEngineUtil;
                            args.Properties["superUserPassword"] = superUserPassword;
                            break;
                        case "messagebox":
                            args.Properties["dialogresult"] = showMessageBox(null, args.Properties["text"] as string, args.Properties["caption"] as string, (MessageBoxButtons)args.Properties["buttons"], (MessageBoxIcon)args.Properties["icon"]);
                            break;
                        default:
                            EventLog.WriteEntry("GRIN-Global Database", "Undefined 'prompt' type in background worker: " + promptType, EventLogEntryType.Error);
                            throw new InvalidOperationException(getDisplayMember("worker_ProgressChanged", "Undefined 'prompt' type in background worker: {0}", promptType));
                    }
                }
            } finally {
                if (unblockThread) {
                    Toolkit.UnblockBlockedThread();
                }
            }
        }

        private string _dbLogFile;
        private void updateProgressForm(string text, bool showMessageBoxOnError) {
            try {
                var msg = DateTime.Now.ToString() + " - " + text + "\r\n";
                _frmProgress.txtProgress.AppendText(msg);
                              
                //_frmProgress.txtProgress.SelectionStart = _frmProgress.txtProgress.Text.Length;
                //_frmProgress.txtProgress.ScrollToCaret();
                if (_dbLogFile == null) {
                    _dbLogFile = Toolkit.ResolveFilePath(Utility.GetTargetDirectory(Context, null, "Database") + @"\db_install_log.txt", true);
                }
                File.AppendAllText(_dbLogFile, msg);
                _frmProgress.Refresh();
                Application.DoEvents();
            } catch {
                // window may be gone by the time we get here.  Ignore it if it is since this is just informational for the user anyway
            }
            if (showMessageBoxOnError) {
                if (text.ToLower().Contains("error")) {
                    //Context.LogMessage(text);
                    EventLog.WriteEntry("GRIN-Global Database", "Error message to user: " + text, EventLogEntryType.Error);
                    MessageBox.Show(text, getDisplayMember("updateProgressForm", "Error"));
                    throw new Exception(text);
                }
            }
        }

        private void initProgressForm() {
            if (_frmProgress == null) {
                _frmProgress = new frmProgress();
                _frmProgress.btnDone.Visible = false;
                _frmProgress.Cursor = Cursors.WaitCursor;
                _frmProgress.Show();
                updateProgressForm("Initializing, please be patient...", false);
                Toolkit.ActivateApplication(_frmProgress.Handle);
                Application.DoEvents();
            }
        }

        private void initContextVariables(BackgroundWorker worker, StringBuilder sb, InstallEventArgs e, ref string engine, ref string instanceName, ref string databaseName, ref bool windowsAuth, ref DatabaseEngineUtil dbEngineUtil, ref string targetDir, ref string sourceDir, ref string gguacPath, ref string dataDestinationFolder, ref string superUserPassword, bool allowSkip) {
            // init variables
            if (engine == null) {
                engine = Utility.GetDatabaseEngine(this.Context, e.SavedState, false, "");
            }
            sb.AppendLine("engine=" + engine);
            if (instanceName == null) {
                instanceName = Utility.GetDatabaseInstanceName(this.Context, e.SavedState, "");
            }
            sb.AppendLine("instance=" + instanceName);

            if (databaseName == null)
            {
                databaseName = Utility.GetDatabaseName(this.Context, e.SavedState, "gringlobal");
            }
            sb.AppendLine("databasename=" + databaseName);

            if (targetDir == null) {
                targetDir = Utility.GetTargetDirectory(this.Context, e.SavedState, "Database");
            }
            sb.AppendLine("targetdir=" + targetDir);
            if (sourceDir == null) {
                sourceDir = Utility.GetSourceDirectory(this.Context, e.SavedState, null);
            }
            sb.AppendLine("sourcedir=" + sourceDir);
            if (gguacPath == null) {
                gguacPath = Toolkit.ResolveFilePath(targetDir + @"\gguac.exe", false);
            }
            sb.AppendLine("gguacPath=" + gguacPath);
            if (dataDestinationFolder == null) {
                dataDestinationFolder = (Utility.GetTempDirectory(1500) + @"\data").Replace(@"\\", @"\");
            }
            sb.AppendLine("data temp folder=" + dataDestinationFolder);
            if (superUserPassword == null) {
                superUserPassword = Utility.GetSuperUserPassword(Context, e.SavedState);
            }
            sb.AppendLine("superuser db password specified? " + (String.IsNullOrEmpty(superUserPassword) ? "Not yet" : "Yes"));
            windowsAuth = Utility.GetDatabaseWindowsAuth(Context, e.SavedState);
            sb.AppendLine("windowsauth=" + windowsAuth.ToString());

            var useRegSettings = Utility.GetUseRegSettings(Context, e.SavedState);
            if (dbEngineUtil == null) {
                if (!DatabaseEngineUtil.IsValidEngine(engine)) {
                    // default to sql server,  SQLExpress instance
                    engine = "sqlserver";
                //    windowsAuth = true;
                    instanceName = "MSSQLSERVER";
                }

                // prompt the user to tell us which engine and connection information they want us to use in case they want to change it
                var resp = promptForDatabaseLoginInfo(worker, engine, instanceName, windowsAuth,  gguacPath, ref dbEngineUtil, ref superUserPassword, allowSkip);
                if (resp == DialogResult.OK){
                    engine = dbEngineUtil.EngineName;
                    sb.AppendLine("engine=" + engine);
                    if (dbEngineUtil is SqlServerEngineUtil) {
                        instanceName = (dbEngineUtil as SqlServerEngineUtil).InstanceName;
                        sb.AppendLine("instanceName=" + instanceName);
                    }
                    windowsAuth = dbEngineUtil.UseWindowsAuthentication;
                    sb.AppendLine("windowsauth=" + windowsAuth);
                    //setSuperUserPassword(e.SavedState, superUserPassword);
                    sb.AppendLine("superuser db password specified? " + (String.IsNullOrEmpty(superUserPassword) ? "Not yet" : "Yes"));

                } else if (resp == DialogResult.Ignore){
                    dbEngineUtil = null;
                } else {
                    throw new InvalidOperationException(getDisplayMember("initContextVariables{usercancel}", "User cancelled out of database connection dialog"));
                }

            }

            // read resonse value stashed in registry
            databaseName = Utility.GetDatabaseName(this.Context, e.SavedState, "gringlobal");
        }

        /// <summary>
        /// inner class used for negotiating between main and background threads at start and end of background thread execution
        /// </summary>
        private class WorkerArguments {
            public Exception Exception;
            public InstallEventArgs InstallEventArgs;
            public Dictionary<string, object> Properties;
            public WorkerArguments() {
                Properties = new Dictionary<string, object>();
            }
        }

        private void DatabaseInstaller_AfterInstall(object sender, InstallEventArgs e) {

            try {
                initProgressForm();

                int installerWindowHandle = Toolkit.GetWindowHandle("GRIN-Global Database");
                if (installerWindowHandle > 0) {
                    Toolkit.MinimizeWindow(installerWindowHandle);
                }

                using (var worker = initWorker()) {
                    var wa = new WorkerArguments { InstallEventArgs = e };
                    worker.DoWork += new DoWorkEventHandler(installworker_DoWork);
                    try {
                        worker.RunWorkerAsync(wa);
                        waitForWorkerToComplete(worker);
                    } catch (Exception ex) {
                        MessageBox.Show(getDisplayMember("afterInstall{exceptionbody}", "Exception running worker async: {0}", ex.Message), getDisplayMember("afterInstall{exceptiontitle}", "Worker Launch Exception"));
                    } finally {
                        if (installerWindowHandle > 0) {
                            Toolkit.RestoreWindow(installerWindowHandle);
                        }

                        // exceptions thrown in the background worker thread won't cause us to bomb here.
                        // so the code running in background worker context needs to tell us if it encountered an exception.
                        // we rethrow it on the primary process thread here if needed.
                        if (wa.Exception != null) {
                            throw wa.Exception;
                        }
                    }
                }
            } catch (Exception ex){
                EventLog.WriteEntry("GRIN-Global Database", Toolkit.Cut("Exception in DatabaseInstaller_AfterInstall: " + ex.Message, 0, 8000), EventLogEntryType.Error);
                throw;
            } finally {
                _workerDone = true;
            }

        }

        void installworker_DoWork(object sender, DoWorkEventArgs dwea) {
            StringBuilder sb = new StringBuilder();
            var wa = dwea.Argument as WorkerArguments;
            var worker = sender as BackgroundWorker;
            var e = wa.InstallEventArgs;

            try {
                doInstall(sender as BackgroundWorker, sb, (dwea.Argument as WorkerArguments));
            } finally {
                progressDone();
            }
        }

        void doInstall(BackgroundWorker worker, StringBuilder sb, WorkerArguments wa) {

            var e = wa.InstallEventArgs;

            // showMessageBox("Running in background = " + Thread.CurrentThread.IsBackground, "Background thread?", MessageBoxButtons.OK, MessageBoxIcon.Information);

            try {

                worker.ReportProgress(0, getDisplayMember("doInstall{verifying}", "Verifying database credentials..."));

                // init vars
                string engine = null;
                string instanceName = null;
                string databaseName = null;
                bool windowsAuth = false;
                DatabaseEngineUtil dbEngineUtil = null;
                string targetDir = null;
                string sourceDir = null;
                string gguacPath = null;
                string dataDestinationFolder = null;
                string superUserPassword = null;
                //worker.ReportProgress(0, getDisplayMember("doInstall{kfe1}", "Before initContextVariables databaseName = {0}", databaseName));
                initContextVariables(worker, sb, e, ref engine, ref instanceName, ref databaseName, ref windowsAuth, ref dbEngineUtil, ref targetDir, ref sourceDir, ref gguacPath, ref dataDestinationFolder, ref superUserPassword, false);
                //worker.ReportProgress(0, getDisplayMember("doInstall{kfe2}", "After initContextVariables databaseName = {0}", databaseName));

                // pluck data source db name found after < in database name - a kludge to avoid another field on the form
                string fromDbName = null;
                int i;
                i = databaseName.IndexOf('<');
                if (i > -1) {
                    fromDbName = databaseName.Substring(i+1).TrimStart();
                    databaseName = databaseName.Substring(0, i).TrimEnd();
                }

                // prepare the x64-specific exe in case we need to use it to look up registry keys that can only be retrieved from a 64-bit process on a 64-bit OS
                worker.ReportProgress(0, getDisplayMember("doInstall{extracting}", "Extracting utilities..."));
                extractUtility64File(targetDir, gguacPath);

                // prepare to talk to the database engine

                //determineSuperUserPassword(dbEngineUtil, instanceName, ref superUserPassword);
                string connectionString = dbEngineUtil.GetDataConnectionSpec(databaseName, "__USERID__", "__PASSWORD__").ConnectionString;

                // make sure no previous data is pulled in to current data load
                Toolkit.EmptyDirectory(dataDestinationFolder);

                // extract system data that was included in the msi
                extractSystemDataFile(worker, sb, targetDir, gguacPath, dataDestinationFolder);

                // 2010-10-14 Brock Weaver brock@circaware.com
                // disabled, no longer necessary for testing...
                // prompt for additional data
                promptForAndExtractAdditionalData(worker, sb, dataDestinationFolder, gguacPath);


                // create and populate database
                worker.ReportProgress(0, getDisplayMember("doInstall{starting}", "Starting install to database engine={0}, service={1}, connection string={2} ...", dbEngineUtil.EngineName, dbEngineUtil.ServiceName, connectionString));
           
                sb.AppendLine("Creating "+databaseName+" database");
                bool doingDbUpgrade = false;
                bool ask = true;
                try {

                    createDatabase(worker, dbEngineUtil, targetDir, superUserPassword, databaseName);

                } catch (Exception exCreate) {
                    if (!exCreate.Message.ToLower().Contains("already exists. choose a different database name.")) {
                        // let exceptions other than database already exists continue on up
                        throw;
                    }

                    EventLog.WriteEntry("GRIN-Global Database", "Exception creating database: " + exCreate.Message, EventLogEntryType.Error);

                    var dr = showMessageBox(worker,
                        getDisplayMember("createDatabase{dbalreadyexistsbody}", @"Database {0} already exists. Do you want to upgrade it?

    No will overwrite and Cancel will exit", databaseName),
                        getDisplayMember("createDatabase{dbalreadyexiststitle}", "Database Exists"),
                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
                    if (dr == DialogResult.Cancel) {
                        // let the exists exception kill the install
                        throw;
                    } else if (dr == DialogResult.Yes) {
                        doingDbUpgrade = true;

                        string question = getDisplayMember("createDatabase{autoupgradebody}", @"YES to ALL upgrade questions?

No will prompt for each step");
                        string title = getDisplayMember("createDatabase{autoupgradetitle}", "Automatic Upgrade");
                        if (showMessageBox(worker, question, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                            ask = false;
                        } else {
                            ask = true;
                        }

                    } else {
                        dr = showMessageBox(worker,
                        getDisplayMember("createDatabase{dboverwritebody}", "Do you want to overwrite database {0} with a new empty database?", databaseName),
                        getDisplayMember("createDatabase{dboverwritetitle}", "Database Overwrite"),
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes) {
                            doingDbUpgrade = false;
                            bool uninstalledSuccessfully = true;
                            try {
                                uninstalledSuccessfully = doUninstall(worker, sb, dbEngineUtil, false, ref superUserPassword, e);
                            } catch { } // eat any errors from uninstall
                            if (!uninstalledSuccessfully) throw new ApplicationException(getDisplayMember("doInstall{uninstallfailed}", "Uninstall of previous version of GRIN-Global database failed.\nPlease uninstall it manually by going to Add/Remove Programs."));
                            
                            // try creating the database again
                            createDatabase(worker, dbEngineUtil, targetDir, superUserPassword, databaseName);

                        } else {
                            // If they don't want to upgrade or unistall, let the exception cancel the install
                            throw;
                        }
                    }
                }

                // KMK 10/6/2017  DB install bombs at times because the newly created db is not accessible
                //Added code to check if database is accessible
                string available = CheckDBAvailability(databaseName, superUserPassword, dbEngineUtil);
                if (available == null) throw new ApplicationException(getDisplayMember("createDatabase{createDatabsefailed}", "Creation of GRIN-Global database may have failed.\nPlease try running the installer again."));

                Creator c = null;
                List<TableInfo> tables = null;

                if (!doingDbUpgrade) { 

                    // normal install
                    // create tables / fill with data / create users / create indexes / create constraints
                    populateDatabase(worker, sb, dbEngineUtil, ref c, ref tables, dataDestinationFolder, targetDir, superUserPassword, databaseName, fromDbName);

                    // create sequences / triggers as needed
                    createDependentDatabaseObjects(worker, sb, dbEngineUtil, c, tables, targetDir, superUserPassword, databaseName);

                    // perform any clean up
                    finalizeDatabaseInstall(worker, sb, dbEngineUtil, c, tables, superUserPassword, databaseName, targetDir);


                } else { 
                    // upgrade time!
                    c = Creator.GetInstance(dbEngineUtil.GetDataConnectionSpec(databaseName, dbEngineUtil.SuperUserName, superUserPassword));
                    // c.OnProgress += new ProgressEventHandler(c_OnProgress);

                    // load up to date information from __schema.xml
                    sb.AppendLine("Reading new schema information");
                    worker.ReportProgress(0, getDisplayMember("populateDatabase{readingnewschema}", "Reading new schema information..."));
                    tables = c.LoadTableInfo(dataDestinationFolder);

                    // ugrade the database schema
                    upgradeDatabase(worker, sb, dbEngineUtil, c, tables, targetDir, superUserPassword, databaseName, ask);

                    // make a list of system tables to load temporarily for comparison
                    List<TableInfo> tempSysTables = new List<TableInfo>();
                    foreach (TableInfo ti in tables) {
                        if (ti.TableName.StartsWith("sys_")
                            || ti.TableName.StartsWith("code_")
                            || ti.TableName.StartsWith("app_")) {
                                tempSysTables.Add(ti);
                        }
                    }
                    string tempPrefix = "temp__";
                    foreach (TableInfo ti in tempSysTables) {
                        ti.TableName = tempPrefix + ti.TableName;
                        //ti.IsSelected = true;
                    }

                    Thread.Sleep(100);
                    sb.AppendLine("Creating temp tables");
                    worker.ReportProgress(0, getDisplayMember("populateDatabase{creatingtemptables}", "Creating temp tables..."));
                    Thread.Sleep(100);
                    // create the temporary system tables
                    c.CreateTables(tempSysTables, databaseName);

                    Thread.Sleep(100);
                    sb.AppendLine("Populating temp tables");
                    worker.ReportProgress(0, getDisplayMember("populateDatabase{populatingtemptables}", "Populating temp tables..."));
                    // load the up to date system information to the temporary tables
                    c.CopyDataToDatabase(dataDestinationFolder, databaseName, tempSysTables, false, false);

                    Thread.Sleep(100);
                    sb.AppendLine("Ready to copy dataviews");
                    worker.ReportProgress(0, getDisplayMember("populateDatabase{populatingtemptables}", "Ready to copy dataviews..."));
                    // copy new and updated dataviews from the temporary system tables
                    CopyDvsBetweenDatabases(worker, sb, dbEngineUtil, c, dataDestinationFolder, targetDir, superUserPassword, "", tempPrefix, ask);

                    Thread.Sleep(100);
                    sb.AppendLine("Ready to copy stfl");
                    worker.ReportProgress(0, getDisplayMember("populateDatabase{populatingtemptables}", "Ready to copy stfl..."));
                    // copy new and updated sys_table_field_lang rows from the temporary system tables
                    CopyStflBetweenDatabases(worker, sb, dbEngineUtil, c, dataDestinationFolder, targetDir, superUserPassword, "", tempPrefix, ask);

                    Thread.Sleep(100);
                    sb.AppendLine("Ready to copy dt");
                    worker.ReportProgress(0, getDisplayMember("populateDatabase{populatingtemptables}", "Ready to copy datatrigger..."));
                    // copy new and updated datatrigger rows from the temporary system tables
                    CopyDtBetweenDatabases(worker, sb, dbEngineUtil, c, dataDestinationFolder, targetDir, superUserPassword, "", tempPrefix, ask);

                    Thread.Sleep(100);
                    sb.AppendLine("Ready to copy cvl");
                    worker.ReportProgress(0, getDisplayMember("populateDatabase{populatingtemptables}", "Ready to copy code_value_lang..."));
                    // copy new and updated datatrigger rows from the temporary system tables
                    CopyCvlBetweenDatabases(worker, sb, dbEngineUtil, c, dataDestinationFolder, targetDir, superUserPassword, "", tempPrefix, ask);

                    Thread.Sleep(100);
                    sb.AppendLine("Done updating the database");
                    worker.ReportProgress(0, getDisplayMember("populateDatabase{populatingtemptables}", "Done Updating the {0} database...", databaseName));
                    Thread.Sleep(2000);

                    DialogResult dr = DialogResult.Yes;
                    if (ask) dr = showMessageBox(worker,
                        getDisplayMember("createDatabase{dbdeltempbody}", "Do you want to delete the temp tables used to upgrade?", databaseName),
                        getDisplayMember("createDatabase{dbdeltemptitle}", "Clean Temp Tables"),
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes) {
                        c.DropTables(tempSysTables);
                    }

                }


            } catch (Exception ex) {

                try {
                    sb.AppendLine("Current Directory: " + Environment.CurrentDirectory);
                    sb.AppendLine("Current Username: " + DomainUser.Current);
                    //showMessageBox(worker, "Error during install: " + ex.Message, "Installation Failed", MessageBoxButtons.OK, MessageBoxIcon.Error); // + "\n\nHistory: " + sb.ToString(), "Exception during install", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    var msg = "Exception: " + ex.Message + "\n\nHistory: " + sb.ToString() + "\nException stack: " + ex.ToString(true);
                    EventLog.WriteEntry("GRIN-Global Database", Toolkit.Cut(msg, 0, 8000), EventLogEntryType.Error);
                } catch { 
                    // eat all errors writing to the event log
                }


                var exWrapper = new ApplicationException("Exception: " + ex.Message, ex);
                waitForUserInput(worker, exWrapper, int.MaxValue); // wait forever?
                wa.Exception = ex;
                return;
            }


        }

       
        private void DatabaseInstaller_BeforeUninstall(object sender, InstallEventArgs e) {

            try {
                initProgressForm();


                // HACK: try to give MSI form a moment to focus before we show our dialog...
                Thread.Sleep(1000);
                Application.DoEvents();
                Thread.Sleep(500);

                int installerWindowHandle = Toolkit.GetWindowHandle("GRIN-Global Database");
                if (installerWindowHandle > 0) {
                    Toolkit.MinimizeWindow(installerWindowHandle);
                }


                using (var worker = initWorker()) {
                    var wa = new WorkerArguments { InstallEventArgs = e };
                    worker.DoWork += new DoWorkEventHandler(uninstallworker_DoWork);
                    try {
                        worker.RunWorkerAsync(wa);
                        waitForWorkerToComplete(worker);
                    } finally {
                        try {
                            if (installerWindowHandle > 0) {
                                Toolkit.RestoreWindow(installerWindowHandle);
                            }
                        } catch { }
                        // exceptions thrown in the background worker thread won't cause us to bomb here.
                        // so the code running in background worker context needs to tell us if it encountered an exception.
                        // we rethrow it on the primary process thread here if needed.
                        if (wa.Exception != null) {
                            throw wa.Exception;
                        }
                    }
                }
            } catch (Exception ex) {
                EventLog.WriteEntry("GRIN-Global Database", Toolkit.Cut("Exception in DatabaseInstaller_BeforeUninstall: " + ex.Message, 0, 8000), EventLogEntryType.Error);
                throw;
            } finally {
                _workerDone = true;
            }
            
        }

        void uninstallworker_DoWork(object sender, DoWorkEventArgs dwea) {
            StringBuilder sb = new StringBuilder();
            string superUserPassword = null;
            try {
                // uninstall previous version if needed
                doUninstall(sender as BackgroundWorker, sb, null, true, ref superUserPassword, (dwea.Argument as WorkerArguments).InstallEventArgs);
            } catch (Exception ex) {
                // eat any errors from uninstall
            } finally {
                progressDone();
            }

        }

        private bool IsUpgrade {
            get {
                return !string.IsNullOrEmpty(this.Context.Parameters["oldproductcode"]);
            }
        }


        private bool doUninstall(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, bool atUninstallTime, ref string superUserPassword, InstallEventArgs e) {
            //showState(e.SavedState);

            try {

                //if (atUninstallTime && !IsUpgrade) {
                //    var dr1 = showMessageBox(worker, "Uninstalling the GRIN-Global Database software does not require you to delete the corresponding database and its data.\nDo you want to permanently delete all GRIN-Global data?\n\nAnswering Yes or No will still uninstall this software.", "Delete GRIN-Global Data?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                //    switch (dr1) {
                //        case DialogResult.Yes:
                //            // just continue with the uninstall as normal
                //            break;
                //        case DialogResult.No:
                //            // skip all the database removal, let them keep their data as they asked
                //            return true;
                //        case DialogResult.Cancel:
                //        default:
                //            throw new InvalidOperationException("User cancelled out of delete data prompt.");
                //    }
                //}

                worker.ReportProgress(0, getDisplayMember("doUninstall{verifying}", "Verifying database credentials..."));
                // init vars
                string engine = null;
                string instanceName = null;
                string databaseName = null;
                bool windowsAuth = false;
                string targetDir = null;
                string sourceDir = null;
                string gguacPath = null;
                string dataDestinationFolder = null;
                initContextVariables(worker, sb, e, ref engine, ref instanceName, ref databaseName, ref windowsAuth, ref dbEngineUtil, ref targetDir, ref sourceDir, ref gguacPath, ref dataDestinationFolder, ref superUserPassword, atUninstallTime);

                // bounce db engine to make sure all existing connections are severed
                //restartDatabaseEngine(worker, sb, dbEngineUtil, engine, instanceName, windowsAuth, superUserPassword, e);


                if (dbEngineUtil == null && atUninstallTime) {
                    // user chose to 'skip' database stuff.
                    // so, we don't try to do any database user or data removal
                    EventLog.WriteEntry("GRIN-Global Database", "User chose to skip removing the database at uninstall time.", EventLogEntryType.Information);
                    return true;
                }

                string output = "";

                if (dbEngineUtil is PostgreSqlEngineUtil)
                {
                    // Kill session
                    string killSessionSql = "SELECT pg_terminate_backend(pg_stat_activity.procpid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'gringlobal';";
                    dbEngineUtil.ExecuteSql(superUserPassword, "postgres", killSessionSql);
                    // Drop database
                    dropDatabase(worker, sb, dbEngineUtil, targetDir, superUserPassword, databaseName);
                    // Drop users
                    dropDatabaseUsers(worker, sb, dbEngineUtil, superUserPassword, databaseName);
                }
                else
                {
                    dropDatabaseUsers(worker, sb, dbEngineUtil, superUserPassword, databaseName);

                    // launch database-aware scripts for dropping database
                    try
                    {
                        output = dropDatabase(worker, sb, dbEngineUtil, targetDir, superUserPassword, databaseName);
                    }
                    catch (Exception exDrop)
                    {
                        if (atUninstallTime)
                        {
                            //Context.LogMessage("Exception dropping database: " + exDrop.Message);
                            EventLog.WriteEntry("GRIN-Global Database", "Exception dropping database: " + exDrop.Message, EventLogEntryType.Error);
                            //this.Rollback(e.SavedState);
                            throw;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                // bounce db engine to make sure all existing connections are severed
                // restartDatabaseEngine(worker, sb, dbEngineUtil, engine, instanceName, windowsAuth, superUserPassword, e);

                // delete the utility64 file, if any
                if (atUninstallTime) {
                    try {
                        var utility64exe = Toolkit.ResolveFilePath(targetDir + @"\ggutil64.exe", false);
                        if (File.Exists(utility64exe)) {
                            File.Delete(utility64exe);
                        }
                    } catch {
                        // eat any errors
                    }
                }

            } catch (Exception ex2) {
                // Context.LogMessage("Exception: " + ex2.Message);
                if (atUninstallTime) {
                    //this.Rollback(e.SavedState);
                    // MessageBox.Show("The following error occurred when attempting to uninstall GRIN-Global Database: " + ex2.Message + "\n\nHowever, uninstallation will continue.");
                    // throw;
                    EventLog.WriteEntry("GRIN-Global Database", "Error uninstalling database: " + ex2.Message, EventLogEntryType.Error);
                    waitForUserInput(worker, new ApplicationException(getDisplayMember("doUninstall{atuninstalltime}", "The following error occurred when attempting to uninstall GRIN-Global Database: {0}\n\nHowever, uninstallation will continue.", ex2.Message), ex2), int.MaxValue); // wait forever?
                    return false;
                } else {
                    waitForUserInput(worker, new ApplicationException(ex2.Message, ex2), int.MaxValue);  // wait forever?
                    return false;
                }
            }

            return true;

        }

        private void extractUtility64File(string targetDir, string gguacPath) {
            var utility64CabPath = Toolkit.ResolveFilePath(targetDir + @"\utility64.cab", false);
            var utility64ExePath = Toolkit.ResolveFilePath(targetDir + @"\ggutil64.exe", false);

            var tempPath = Utility.GetTempDirectory(15);

            if (!File.Exists(utility64ExePath)) {
                if (File.Exists(utility64CabPath)) {
                    // wipe out any existing utility64.exe file in the temp folder
                    var extracted = Toolkit.ResolveFilePath(tempPath + @"\ggutil64.exe", false);
                    if (File.Exists(extracted)) {
                        File.Delete(extracted);
                    }
                    var extracted2 = Toolkit.ResolveFilePath(tempPath + @"\ggutil64.pdb", false);
                    if (File.Exists(extracted2)) {
                        File.Delete(extracted2);
                    }
                    // extract it from our cab
                    var cabOutput = Utility.ExtractCabFile(utility64CabPath, tempPath, gguacPath);
                    // move it to the final target path (we can't do this up front because expand.exe tells us "can't expand cab file over itself" for some reason.
                    if (File.Exists(extracted)) {
                        File.Move(extracted, utility64ExePath);
                    }
                }
            }
        }

        private void finalizeDatabaseInstall(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, Creator c, List<TableInfo> tables, string superUserPassword, string databaseName , string targetDir) {
            // each database engine has its own nuances after dumping a bunch of data into it...
            if (dbEngineUtil is MySqlEngineUtil) {
                // rebuild the indexes
                sb.AppendLine("Rebuilding indexes");
                worker.ReportProgress(0, getDisplayMember("finalizeDatabaseInstall{rebuilding}", "Rebuilding indexes..."));
                c.RebuildIndexes(tables);

                if ((dbEngineUtil.Version + "").Trim().StartsWith("5.")) {

                    // mysql versions < 6.0 do not support unique indexes with null columns properly.
                    // so we fudge this by creating triggers to throw errors when data violates the unique index
                    // http://www.brokenbuild.com/blog/2006/08/15/mysql-triggers-how-do-you-abort-an-insert-update-or-delete-with-a-trigger/

                    worker.ReportProgress(0, getDisplayMember("finalizeDatabaseInstall{triggers}", "Creating triggers to compensate for nullable unique indexes..."));
                    dbEngineUtil.ExecuteSqlFile(superUserPassword, databaseName, Toolkit.ResolveFilePath(targetDir + @"\mysql_create_triggers.sql", false)); 

                }

            } else if (dbEngineUtil is SqlServerEngineUtil) {

                worker.ReportProgress(0, getDisplayMember("finalizeDatabaseInstall{triggers}", "Creating database functions..."));
                try {
                    dbEngineUtil.ExecuteSqlFile(superUserPassword, databaseName, Toolkit.ResolveFilePath(targetDir + @"\sqlserver\create_functions.sql", false));
                } catch { }

            } else if (dbEngineUtil is OracleEngineUtil) {

                // Oracle doesn't need to do anything here...

            } else if (dbEngineUtil is PostgreSqlEngineUtil) {

                // Postgresql doesn't need to do anything here...
            }

        }

        private void upgradeDatabase(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, Creator c, List<TableInfo> tables, string targetDir, string superUserPassword, string databaseName, bool ask) {
            if (dbEngineUtil is SqlServerEngineUtil) {

                Thread.Sleep(100);
                sb.AppendLine("Reading existing schema information");
                worker.ReportProgress(0, getDisplayMember("upgradeDatabaseInstall{readexistingschema}", "Reading existing schema information..."));

                // load info on existing database
                List<string> tablesOfInterest = new List<string> {"cooperator", "app_setting", "taxonomy_genus", "taxonomy_species", "accession_source", "accession_inv_name", "accession_inv_attach"
                    ,"inventory_action", "inventory_viability", "inventory_viability_data", "inventory_viability_rule", "inventory_quality_status", "literature"};
                //var currTables = c.LoadTableInfo(databaseName, tablesOfInterest, false, 0);
                var currTables = c.LoadTableInfoQuick();

                worker.ReportProgress(0, getDisplayMember("upgradeDatabaseInstall{schema}", "Checking for database schema upgrades..."));

                try {
                    int adminId = 48;

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1090201, "accession_inv_name", "is_web_visible", null, null
                        , targetDir + @"\sqlserver\upgrade_schema_1_9_2.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1090301, "inventory_viability_rule", "seeds_per_replicate"
                        , new List<string> {"seeds", "replicates", "taxonomy_species_id"}
                        , "Do you want to apply the version 1.9.3 schema updates to rename and resize a few columns"
                        , targetDir + @"\sqlserver\upgrade_schema_1_9_3.sql", ask);

                    // That script changed a couple other tables so let's remap those as well
                    var ainTi = tables.Find(item => item.TableName == "accession_inv_name");
                    c.CreateTableMappings(ainTi, adminId, tables);
                    var asTi = tables.Find(item => item.TableName == "app_setting");
                    c.CreateTableMappings(asTi, adminId, tables);
                    var cTi = tables.Find(item => item.TableName == "cooperator");
                    c.CreateTableMappings(cTi, adminId, tables);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1090701, "inventory_action", "", new List<string> {"action_date", "action_date_code"}, null
                        , targetDir + @"\sqlserver\upgrade_schema_1_9_7_ia.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1090702, "taxonomy_genus", "", new List<string> { "is_hybrid" }, null
                        , targetDir + @"\sqlserver\upgrade_schema_1_9_7_ctg.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1090901, "accession_source", "associated_species", null, null
                        , targetDir + @"\sqlserver\upgrade_schema_1090901.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1090902, "inventory_viability", "percent_hard", null, null
                        , targetDir + @"\sqlserver\upgrade_schema_1090902.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1090903, "inventory_viability_data", "hard_count", null, null
                        , targetDir + @"\sqlserver\upgrade_schema_1090903.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1090904, "inventory_viability_rule", "moisture", new List<string> { "seeds", "replicates", "taxonomy_species_id" }, ""
                        , targetDir + @"\sqlserver\upgrade_schema_1090904.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1100101, "taxonomy_species", "protologue_virtual_path", null, null
                        , targetDir + @"\sqlserver\upgrade_schema_1_10_1.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1100201, "inventory_quality_status", "negative_control", null, null
                        , targetDir + @"\sqlserver\upgrade_schema_1_10_2_iqs.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1100202, "literature", "publication_year", null, null
                        , targetDir + @"\sqlserver\upgrade_schema_1_10_2_l.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1100203, "accession_inv_attach", "description_code", null, null
                        , targetDir + @"\sqlserver\upgrade_schema_1_10_2_aia.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1100301, "citation", "is_accepted_name", null, null
                        , targetDir + @"\sqlserver\upgrade_schema_1100301.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1100302, "accession_inv_group_attach", "virtual_path", null
                        , "Do you want to add the accession_inv_group_attach table"
                        , targetDir + @"\sqlserver\upgrade_schema_1100302.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1100303, "method_attach", "virtual_path", null
                        , "Do you want to add the method_attach table"
                        , targetDir + @"\sqlserver\upgrade_schema_1100303.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1100401, "geography", "is_valid", null, null
                        , targetDir + @"\sqlserver\upgrade_schema_1100401.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1100402, "accession", "doi", null, null
                        , targetDir + @"\sqlserver\upgrade_schema_1100402.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1100403, "taxonomy_cwr_priority", "taxonomy_cwr_priority_id", null
                        , "Do you want to add the taxonomy_cwr_priority table"
                        , targetDir + @"\sqlserver\upgrade_schema_1100403.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1100404, "accession_inv_group", "method_id", null, null
                        , targetDir + @"\sqlserver\upgrade_schema_1100404.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1100405, "order_request_item_action", "order_request_item_action_id", null
                        , "Do you want to add the order_request_item_action table"
                        , targetDir + @"\sqlserver\upgrade_schema_1100405.sql", ask);

                    upgradeTableSchema(worker, sb, dbEngineUtil, c, currTables, tables, targetDir, superUserPassword, databaseName
                        , 1100406, "accession_inv_voucher", "ndx_uniq_aiv", null
                        , "Do you want to add the collector_voucher_number to the accession_inv_voucher unique index"
                        , targetDir + @"\sqlserver\upgrade_schema_1100406.sql", ask);

                } catch {
                    worker.ReportProgress(0, getDisplayMember("upgradeDatabaseInstall{fail}", "Failed to upgrade the database schema..."));
                    return;
                }

                worker.ReportProgress(0, getDisplayMember("upgradeDatabaseInstall{schemadone}", "Finished upgrading the database schema..."));
            }
        }

        private void compareFieldsInTable(List<TableInfo> currTables, List<TableInfo> newTables, string tableName, out List<string> newFields, out List<string> missFields, out List<string> diffFields, out List<string> sameFields) {
            newFields = new List<string>();
            missFields = new List<string>();
            diffFields = new List<string>();
            sameFields = new List<string>();

            var currTi = currTables.Find(item => item.TableName == tableName);
            var newTi = newTables.Find(item => item.TableName == tableName);
            if (currTi == null || newTi == null) return;

            foreach (FieldInfo newFi in newTi.Fields) {
                var currFi = currTi.Fields.Find(item => item.Name == newFi.Name);
                if (currFi == null) {
                    newFields.Add(newFi.Name);
                } else if (newFi.DataType == currFi.DataType && newFi.MaxLength == currFi.MaxLength && newFi.IsNullable == currFi.IsNullable) {
                    sameFields.Add(newFi.Name);
                } else {
                    diffFields.Add(newFi.Name);
                }
            }

            foreach (FieldInfo currFi in currTi.Fields) {
                var newFi = newTi.Fields.Find(item => item.Name == currFi.Name);
                if (newFi == null) {
                    missFields.Add(currFi.Name);
                }
            }
        }


        private void upgradeTableSchema(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, Creator c, List<TableInfo> currTables, List<TableInfo> newTables, string targetDir, string superUserPassword, string databaseName, int migration, string tableName, string newField, List<string> ignoreFields, string question, string ScriptName, bool ask) {
            string warning = "";
            List<string> newFields = new List<string>();
            List<string> missFields = new List<string>();
            List<string> diffFields = new List<string>();
            List<string> sameFields = new List<string>();

            // Compare the fields in the current and up-to-date versions of the table. Generate a warning if upgrade would lose fields.
            compareFieldsInTable(currTables, newTables, tableName, out newFields, out missFields, out diffFields, out sameFields);

            if (ignoreFields != null) {
                foreach (string ignore in ignoreFields) {
                    if (missFields.Contains(ignore)) missFields.RemoveAt(missFields.IndexOf(ignore));
                }
            }

            if (missFields.Count() == 1) {
                warning += "Your " + tableName + " table has a custom field: " + missFields[0] + "\r\n";
                warning += "\r\nWARNING - If you upgrade you will lose that field!\r\n\r\n";
            } else if (missFields.Count() > 1) { 
                warning += "Your " + tableName + " table has custom fields: " + string.Join(", ", missFields.ToArray()) + "\r\n";
                warning += "\r\nWARNING - If you upgrade you will lose those fields!\r\n\r\n";
            }

            // No need to upgrade if field isn't new to existing database
            if ((!string.IsNullOrEmpty(newField) && sameFields.Contains(newField))
                ||(string.IsNullOrEmpty(newField) && newFields.Count() <= 0)) {
                worker.ReportProgress(0, getDisplayMember("upgradetableschema{noneed}", "No need to upgrade table {0} for migration {1}...", tableName, migration.ToString()));
                return;
            }

            if (ask || !string.IsNullOrEmpty(warning)) {
                if (string.IsNullOrEmpty(question)) question = "Do you want to update the " + tableName + " table using migration " + migration.ToString();
                string message = getDisplayMember("upgradetableschema{body}", "{0}?", warning+question);
                string title = getDisplayMember("upgradetableschema{title}", "Upgrade Table {0}", tableName);
                if (showMessageBox(worker, message, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) {
                    worker.ReportProgress(0, getDisplayMember("upgradetableschema{skipping}", "Skipping Upgrade of table {0} for migration {1}...", tableName, migration.ToString()));
                    return;
                }
            }

            worker.ReportProgress(0, getDisplayMember("upgradetableschema{upgrading}", "Upgrading database schema of table {0} for migration {1}...", tableName, migration.ToString()));
            dbEngineUtil.ExecuteSqlFile(superUserPassword, databaseName, Toolkit.ResolveFilePath(ScriptName, false));

            // remap the altered table
            var ti = newTables.Find(item => item.TableName == tableName);
            c.CreateTableMappings(ti, 1, newTables);

            // note upgrade in sys_database_migration
            using (var dm = DataManager.Create(c.DataConnectionSpec)) {
                string sql = @"INSERT INTO sys_database_migration
(migration_number, sort_order, action_type, action_up, created_date, created_by, owned_date, owned_by)
VALUES (:migno, (select COALESCE(max(sort_order), -1) + 1 FROM sys_database_migration WHERE migration_number = :migno)
,'SQL file', :file, GETUTCDATE(), 1, GETUTCDATE(), 1)";
                dm.Write(sql, new DataParameters(":migno", migration, ":file", ScriptName));

                sql = @"UPDATE sys_database SET migration_number = :migno, modified_date = GETUTCDATE() WHERE sys_database_id = (SELECT MAX(sys_database_id) FROM sys_database)";
                dm.Write(sql, new DataParameters(":migno", migration));

                sql = @"INSERT INTO sys_database_migration_lang
(sys_database_migration_id, language_iso_639_3_code, title, description, created_date, created_by, owned_date, owned_by)
VALUES ((SELECT MAX(sys_database_migration_id) FROM sys_database_migration WHERE migration_number = :migno)
,'ENG', 'Schema update of table ' + :table, 'Automatic update by GG Database Installer using script ' + :file
, GETUTCDATE(), 1, GETUTCDATE(), 1)";
                dm.Write(sql, new DataParameters(":migno", migration, ":table", tableName, ":file", ScriptName));
            }
        }


        public void CopyDvsBetweenDatabases(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, Creator c, string dataDestinationFolder, string targetDir, string superUserPassword, string toTablePrefix, string fromTablePrefix, bool ask) {
            using (DataManager dm = DataManager.Create(c.DataConnectionSpec)) {
                string findSql = @"SELECT sd1.dataview_name, sd1.sys_dataview_id AS from_id, sd2.sys_dataview_id AS to_id
, COALESCE(sd1.modified_date, sd1.created_date) AS from_date
, COALESCE(sd2.modified_date, sd2.created_date) AS to_date
FROM ##TPRE1##sys_dataview sd1
LEFT JOIN ##TPRE2##sys_dataview sd2 ON sd1.dataview_name = sd2.dataview_name
ORDER BY sd1.dataview_name";
                findSql = findSql.Replace("##TPRE1##", fromTablePrefix).Replace("##TPRE2##", toTablePrefix);

                DataTable dt = dm.Read(findSql);
                int totalCnt = dt.Rows.Count;
                int newCnt = dt.Select("to_date IS NULL").Length;
                int newerCnt = dt.Select("to_date < from_date").Length;
                int attempt = 0;
                string where = null;

                if (totalCnt > 0) {
                    string question = getDisplayMember("copyDvs{dvupgradeall}",
                        @"Do you want load all {0} dataviews from the new release?
({1} are new and {2} are updated.)

    No will load just new and updated, Cancel will load none."
                        , totalCnt.ToString(), newCnt.ToString(), newerCnt.ToString());
                    string title = getDisplayMember("copydvs{dvupgradetitle}", "Dataview Upgrade");
                    DialogResult dr = DialogResult.Yes;
                    if (ask) dr = showMessageBox(worker, question, title, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                    if (dr == DialogResult.Yes) {
                        where = null;
                        attempt = totalCnt;
                    } else if (dr == DialogResult.Cancel) {
                        worker.ReportProgress(0, getDisplayMember("copydvs{skipdvupgrade}", "Skipping Dataview Upgrade..."));
                        return;

                    } else { // not everything
                        attempt = newCnt;
                        where = "WHERE sd2.created_date IS NULL";
                        if (newerCnt > 0) {
                            question = getDisplayMember("copydvs{dvupgraderecent}",
                            @"As well as the {0} completely new dataviews, do you want to load the {1} more recent dataviews?

    No will just load the {0} new, Cancel will load none.", newCnt.ToString(), newerCnt.ToString());
                            dr = showMessageBox(worker, question, title, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                            if (dr == DialogResult.Cancel) {
                                worker.ReportProgress(0, getDisplayMember("copydvs{skipdvupgrade}", "Skipping Dataview Upgrade..."));
                                return;
                            } else if (dr == DialogResult.Yes) {
                                attempt = newCnt + newerCnt;
                                where = "WHERE COALESCE(sd1.modified_date, sd1.created_date) > COALESCE(sd2.modified_date, sd2.created_date, '2000-01-01')";
                            }
                        }
                    }

                    worker.ReportProgress(0, getDisplayMember("copydvs{upgradedvs}", "Upgrading {0} dataviews...", attempt.ToString()));

                    string updateSql = @"
If(OBJECT_ID('tempdb..#dvdiff') Is Not Null) Drop Table #dvdiff;

SELECT sd1.dataview_name, sd1.sys_dataview_id AS from_id, sd2.sys_dataview_id AS to_id
INTO #dvdiff
FROM ##TPRE1##sys_dataview sd1
LEFT JOIN ##TPRE2##sys_dataview sd2 ON sd2.dataview_name = sd1.dataview_name
##WHERE##
ORDER BY sd1.dataview_name

-- add new dataviews
INSERT INTO ##TPRE2##sys_dataview
(dataview_name, is_enabled, is_readonly, category_code, database_area_code, database_area_code_sort_order, is_transform, transform_field_for_names, transform_field_for_captions, transform_field_for_values, configuration_options, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
SELECT
sd1.dataview_name, is_enabled, is_readonly, category_code, database_area_code, database_area_code_sort_order, is_transform, transform_field_for_names, transform_field_for_captions, transform_field_for_values, configuration_options,
created_date, created_by, modified_date, modified_by, owned_date, owned_by
FROM ##TPRE1##sys_dataview sd1
JOIN #dvdiff ON #dvdiff.from_id = sd1.sys_dataview_id AND #dvdiff.to_id IS NULL
ORDER BY dataview_name

-- update #dvdiff with new dataview ids
UPDATE #dvdiff
SET to_id = (SELECT sys_dataview_id FROM ##TPRE2##sys_dataview WHERE dataview_name = #dvdiff.dataview_name)
WHERE #dvdiff.to_id IS NULL

-- update dataview table info
UPDATE sd2 SET 
    sd2.dataview_name = sd1.dataview_name,
    sd2.is_enabled = sd1.is_enabled,
    sd2.is_readonly = sd1.is_readonly,
    sd2.category_code = sd1.category_code,
    sd2.database_area_code = sd1.database_area_code,
    sd2.database_area_code_sort_order = sd1.database_area_code_sort_order,
    sd2.is_transform = sd1.is_transform,
    sd2.transform_field_for_names = sd1.transform_field_for_names,
    sd2.transform_field_for_captions = sd1.transform_field_for_captions,
    sd2.transform_field_for_values = sd1.transform_field_for_values,
    sd2.configuration_options = sd1.configuration_options,
    sd2.created_date = sd1.created_date,
    sd2.modified_date = sd1.modified_date,
    sd2.modified_by = sd1.modified_by
    --sd2.owned_date = :now,
    --sd2.owned_by = :who
FROM #dvdiff
JOIN ##TPRE1##sys_dataview sd1 ON sd1.sys_dataview_id = #dvdiff.from_id
JOIN ##TPRE2##sys_dataview sd2 ON sd2.sys_dataview_id = #dvdiff.to_id


-- delete obsolete params
DELETE ##TPRE2##sys_dataview_param
FROM ##TPRE2##sys_dataview_param sdp2
JOIN #dvdiff ON #dvdiff.to_id = sdp2.sys_dataview_id
WHERE NOT EXISTS (SELECT * FROM ##TPRE1##sys_dataview_param WHERE sys_dataview_id = #dvdiff.from_id AND param_name = sdp2.param_name)

-- update matching params
UPDATE sdp2 SET 
    sdp2.param_type = sdp1.param_type,
    sdp2.sort_order = sdp1.sort_order,
    sdp2.modified_date = sdp1.modified_date,
    sdp2.modified_by = sdp1.modified_by
FROM #dvdiff
JOIN ##TPRE1##sys_dataview_param sdp1 ON sdp1.sys_dataview_id = #dvdiff.from_id
JOIN ##TPRE2##sys_dataview_param sdp2 ON sdp2.sys_dataview_id = #dvdiff.to_id AND sdp2.param_name = sdp1.param_name

-- insert missing params
INSERT INTO ##TPRE2##sys_dataview_param
(sys_dataview_id, param_name, param_type, sort_order, created_date, created_by, owned_date, owned_by)
SELECT #dvdiff.to_id, sdp1.param_name, sdp1.param_type, sdp1.sort_order,
sdp1.created_date, sdp1.created_by, sdp1.owned_date, sdp1.owned_by
FROM ##TPRE1##sys_dataview_param sdp1
JOIN #dvdiff ON #dvdiff.from_id = sdp1.sys_dataview_id
WHERE NOT EXISTS (SELECT * FROM ##TPRE2##sys_dataview_param WHERE sys_dataview_id = #dvdiff.to_id and param_name = sdp1.param_name)


-- delete obsolete sql
DELETE ##TPRE2##sys_dataview_param
FROM ##TPRE2##sys_dataview_sql sds2
JOIN #dvdiff ON #dvdiff.to_id = sds2.sys_dataview_id
WHERE NOT EXISTS (SELECT * FROM ##TPRE1##sys_dataview_sql WHERE sys_dataview_id = #dvdiff.from_id AND database_engine_tag = sds2.database_engine_tag)

-- update matching sql
UPDATE sds2 SET 
    sds2.sql_statement = sds1.sql_statement,
    sds2.modified_date = sds1.modified_date,
    sds2.modified_by = sds1.modified_by
FROM #dvdiff
JOIN ##TPRE1##sys_dataview_sql sds1 ON sds1.sys_dataview_id = #dvdiff.from_id
JOIN ##TPRE2##sys_dataview_sql sds2 ON sds2.sys_dataview_id = #dvdiff.to_id AND sds2.database_engine_tag = sds1.database_engine_tag

-- insert missing sql
INSERT INTO ##TPRE2##sys_dataview_sql
(sys_dataview_id, database_engine_tag, sql_statement, created_date, created_by, owned_date, owned_by)
SELECT #dvdiff.to_id, database_engine_tag, sql_statement, created_date, created_by, owned_date, owned_by
FROM ##TPRE1##sys_dataview_sql sds1
JOIN #dvdiff ON #dvdiff.from_id = sds1.sys_dataview_id
WHERE NOT EXISTS (SELECT * FROM ##TPRE2##sys_dataview_sql WHERE sys_dataview_id = #dvdiff.to_id and database_engine_tag = sds1.database_engine_tag)


-- delete lang rows of obsolete fields
DELETE ##TPRE2##sys_dataview_field_lang WHERE sys_dataview_field_id IN (
	SELECT sys_dataview_field_id FROM ##TPRE2##sys_dataview_field sdf2
	JOIN #dvdiff ON #dvdiff.to_id = sdf2.sys_dataview_id
	WHERE NOT EXISTS (SELECT * FROM ##TPRE1##sys_dataview_field WHERE sys_dataview_id = #dvdiff.from_id AND field_name = sdf2.field_name)
)

-- delete obsolete fields
DELETE ##TPRE2##sys_dataview_field WHERE sys_dataview_field_id IN (
	SELECT sys_dataview_field_id FROM ##TPRE2##sys_dataview_field sdf2
	JOIN #dvdiff ON #dvdiff.to_id = sdf2.sys_dataview_id
	WHERE NOT EXISTS (SELECT * FROM ##TPRE1##sys_dataview_field WHERE sys_dataview_id = #dvdiff.from_id AND field_name = sdf2.field_name)
)

-- update matching fields
UPDATE sdf2 SET 
    sdf2.sys_table_field_id = (
		SELECT sys_table_field_id FROM ##TPRE2##sys_table_field stf2 JOIN ##TPRE2##sys_table st2 ON st2.sys_table_id = stf2.sys_table_id
		 WHERE stf2.field_name = (SELECT field_name FROM ##TPRE1##sys_table_field stf1 WHERE sys_table_field_id = sdf1.sys_table_field_id)
		   AND st2.table_name = (SELECT table_name FROM ##TPRE1##sys_table_field stf1 JOIN ##TPRE1##sys_table st1 ON st1.sys_table_id = stf1.sys_table_id
				WHERE sys_table_field_id = sdf1.sys_table_field_id)
	),
    sdf2.is_readonly = sdf1.is_readonly,
    sdf2.is_primary_key = sdf1.is_primary_key,
    sdf2.is_transform = sdf1.is_transform,
    sdf2.sort_order = sdf1.sort_order,
    sdf2.gui_hint = sdf1.gui_hint,
    sdf2.foreign_key_dataview_name = sdf1.foreign_key_dataview_name,
    sdf2.group_name = sdf1.group_name,
    sdf2.table_alias_name = sdf1.table_alias_name,
    sdf2.is_visible = sdf1.is_visible,
    sdf2.configuration_options = sdf1.configuration_options,
    sdf2.modified_date = sdf1.modified_date,
    sdf2.modified_by = sdf1.modified_by
FROM #dvdiff
JOIN ##TPRE1##sys_dataview_field sdf1 ON sdf1.sys_dataview_id = #dvdiff.from_id
JOIN ##TPRE2##sys_dataview_field sdf2 ON sdf2.sys_dataview_id = #dvdiff.to_id AND sdf2.field_name = sdf1.field_name

-- insert missing fields
INSERT INTO ##TPRE2##sys_dataview_field
(sys_dataview_id, field_name, sys_table_field_id, is_readonly, is_primary_key, is_transform, sort_order, gui_hint, foreign_key_dataview_name, group_name, table_alias_name, is_visible, configuration_options, created_date, created_by, owned_date, owned_by)
SELECT #dvdiff.to_id, field_name,
	(SELECT sys_table_field_id FROM ##TPRE2##sys_table_field stf2 JOIN ##TPRE2##sys_table st2 ON st2.sys_table_id = stf2.sys_table_id
		WHERE stf2.field_name = (SELECT field_name FROM ##TPRE1##sys_table_field stf1 WHERE sys_table_field_id = sdf1.sys_table_field_id)
		  AND st2.table_name = (SELECT table_name FROM ##TPRE1##sys_table_field stf1 JOIN ##TPRE1##sys_table st1 ON st1.sys_table_id = stf1.sys_table_id
				WHERE sys_table_field_id = sdf1.sys_table_field_id)
	),
	is_readonly, is_primary_key, is_transform, sort_order, gui_hint, foreign_key_dataview_name, group_name, table_alias_name, is_visible, configuration_options, created_date, created_by, owned_date, owned_by
FROM ##TPRE1##sys_dataview_field sdf1
JOIN #dvdiff ON #dvdiff.from_id = sdf1.sys_dataview_id
WHERE NOT EXISTS (SELECT * FROM ##TPRE2##sys_dataview_field WHERE sys_dataview_id = #dvdiff.to_id and field_name = sdf1.field_name)


-- delete obsolete dv.lang
DELETE ##TPRE2##sys_dataview_lang
FROM ##TPRE2##sys_dataview_lang sdl2
JOIN #dvdiff ON #dvdiff.to_id = sdl2.sys_dataview_id
WHERE NOT EXISTS (SELECT * FROM ##TPRE1##sys_dataview_lang WHERE sys_dataview_id = #dvdiff.from_id AND sys_lang_id = sdl2.sys_lang_id)

-- update matching dv lang
UPDATE sdl2 SET 
    sdl2.title	    	= sdl1.title,
    sdl2.description	= sdl1.description,
    sdl2.modified_date	= sdl1.modified_date,
    sdl2.modified_by	= sdl1.modified_by
FROM #dvdiff
JOIN ##TPRE1##sys_dataview_lang sdl1 ON sdl1.sys_dataview_id = #dvdiff.from_id
JOIN ##TPRE2##sys_dataview_lang sdl2 ON sdl2.sys_dataview_id = #dvdiff.to_id AND sdl2.sys_lang_id = sdl1.sys_lang_id

-- insert missing dv lang
INSERT INTO ##TPRE2##sys_dataview_lang
(sys_dataview_id, sys_lang_id, title, description, created_date, created_by, owned_date, owned_by)
SELECT #dvdiff.to_id, sys_lang_id, title, description, created_date, created_by, owned_date, owned_by
FROM ##TPRE1##sys_dataview_lang sdl1
JOIN #dvdiff ON #dvdiff.from_id = sdl1.sys_dataview_id
WHERE NOT EXISTS (SELECT * FROM ##TPRE2##sys_dataview_lang WHERE sys_dataview_id = #dvdiff.to_id and sys_lang_id = sdl1.sys_lang_id)


-- delete obsolete field lang
DELETE ##TPRE2##sys_dataview_field_lang
FROM ##TPRE2##sys_dataview_field_lang sdfl2
JOIN ##TPRE2##sys_dataview_field sdf2 ON sdf2.sys_dataview_field_id = sdfl2.sys_dataview_field_id
JOIN #dvdiff ON #dvdiff.to_id = sdf2.sys_dataview_id
WHERE NOT EXISTS (SELECT * FROM ##TPRE1##sys_dataview_field_lang sdfl1
JOIN ##TPRE2##sys_dataview_field sdf1 ON sdf1.sys_dataview_field_id = sdfl1.sys_dataview_field_id
WHERE sys_dataview_id = #dvdiff.from_id AND sys_lang_id = sdfl1.sys_lang_id AND field_name = sdf2.field_name)

-- update matching field lang
UPDATE sdfl2 SET 
    sdfl2.title = sdfl1.title,
    sdfl2.description = sdfl1.description,
    sdfl2.modified_date = sdfl1.modified_date,
    sdfl2.modified_by = sdfl1.modified_by
FROM #dvdiff
JOIN ##TPRE1##sys_dataview_field sdf1 ON sdf1.sys_dataview_id = #dvdiff.from_id
JOIN ##TPRE1##sys_dataview_field_lang sdfl1 ON sdfl1.sys_dataview_field_id = sdf1.sys_dataview_field_id
JOIN ##TPRE2##sys_dataview_field sdf2 ON sdf2.sys_dataview_id = #dvdiff.to_id AND sdf2.field_name = sdf1.field_name
JOIN ##TPRE2##sys_dataview_field_lang sdfl2 ON sdfl2.sys_dataview_field_id = sdf2.sys_dataview_field_id AND sdfl2.sys_lang_id = sdfl1.sys_lang_id

-- insert missing field lang
INSERT INTO ##TPRE2##sys_dataview_field_lang
(sys_dataview_field_id, sys_lang_id, title, description, created_date, created_by, owned_date, owned_by)
SELECT (SELECT sys_dataview_field_id FROM ##TPRE2##sys_dataview_field WHERE sys_dataview_id = #dvdiff.to_id AND field_name = sdf1.field_name)
	, sys_lang_id, title, description, sdfl1.created_date, sdfl1.created_by, sdfl1.owned_date, sdfl1.owned_by
FROM ##TPRE1##sys_dataview_field_lang sdfl1
JOIN ##TPRE1##sys_dataview_field sdf1 ON sdf1.sys_dataview_field_id = sdfl1.sys_dataview_field_id
JOIN #dvdiff ON #dvdiff.from_id = sdf1.sys_dataview_id
WHERE NOT EXISTS (SELECT * FROM ##TPRE2##sys_dataview_field_lang sdfl2
	JOIN ##TPRE2##sys_dataview_field sdf2 ON sdf2.sys_dataview_field_id = sdfl2.sys_dataview_field_id
	WHERE sys_dataview_id = #dvdiff.to_id AND sys_lang_id = sdfl1.sys_lang_id AND sdf2.field_name = sdf1.field_name)


-- Done, clean up temp table
If(OBJECT_ID('tempdb..#dvdiff') Is Not Null) Drop Table #dvdiff;";

                    updateSql = updateSql.Replace("##TPRE1##", fromTablePrefix).Replace("##TPRE2##", toTablePrefix).Replace("##WHERE##", where);
                    dm.Write(updateSql);
                }
            }
        }


        public void CopyStflBetweenDatabases(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, Creator c, string dataDestinationFolder, string targetDir, string superUserPassword, string toTablePrefix, string fromTablePrefix, bool ask) {
            using (DataManager dm = DataManager.Create(c.DataConnectionSpec)) {
                string findSql = @"
SELECT st1.table_name, stf1.field_name, stfl1.sys_table_field_lang_id AS from_id, stfl2.sys_table_field_lang_id AS to_id
, COALESCE(stfl1.modified_date, stfl1.created_date) AS from_date
, COALESCE(stfl2.modified_date, stfl2.created_date) AS to_date
FROM ##TPRE1##sys_table st1
JOIN ##TPRE1##sys_table_field stf1 ON stf1.sys_table_id = st1.sys_table_id
JOIN ##TPRE1##sys_table_field_lang stfl1 ON stfl1.sys_table_field_id = stf1.sys_table_field_id
JOIN ##TPRE2##sys_table st2 ON st2.table_name = st1.table_name
JOIN ##TPRE2##sys_table_field stf2 ON stf2.sys_table_id = st2.sys_table_id AND stf2.field_name = stf1.field_name
LEFT JOIN ##TPRE2##sys_table_field_lang stfl2 ON stfl2.sys_table_field_id = stf2.sys_table_field_id AND stfl2.sys_lang_id = stfl1.sys_lang_id
WHERE NOT (COALESCE(stfl1.title,'') = COALESCE(stfl2.title, '') AND COALESCE(stfl1.description,'') = COALESCE(stfl2.description,''))
   OR COALESCE(stfl1.modified_date, stfl1.created_date) > COALESCE(stfl2.modified_date, stfl2.created_date, '2000-01-01')
ORDER BY st1.table_name, stf1.field_name, stfl1.sys_lang_id";
                findSql = findSql.Replace("##TPRE1##", fromTablePrefix).Replace("##TPRE2##", toTablePrefix);

                DataTable dt = dm.Read(findSql);
                int totalCnt = dt.Rows.Count;
                int newCnt = dt.Select("to_date IS NULL").Length;
                int newerCnt = dt.Select("to_date < from_date").Length;
                int attempt = 0;
                string where = null;

                if (totalCnt > 0) {
                    string question = getDisplayMember("copystflbetweendatabases{stflbody}",
                        @"Do you want load all {0} modified table field language rows from the new release?
({1} are new and {2} are updated.)

    No will load just new and updated, Cancel will load none."
                        , totalCnt.ToString(), newCnt.ToString(), newerCnt.ToString());
                    string title = getDisplayMember("copystflbetweendatabases{stfltitle}", "Field Language Update");

                    DialogResult dr = DialogResult.Yes;
                    if (ask) dr = showMessageBox(worker, question, title, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                    if (dr == DialogResult.Cancel) {
                        worker.ReportProgress(0, getDisplayMember("copystflbetweendatabases{skipping}", "Skipping table field language upgrade..."));
                        return;
                    } else if (dr == DialogResult.Yes) {
                        where = @"WHERE NOT (COALESCE(stfl1.title,'') = COALESCE(stfl2.title, '') AND COALESCE(stfl1.description,'') = COALESCE(stfl2.description,''))
   OR COALESCE(stfl1.modified_date, stfl1.created_date) > COALESCE(stfl2.modified_date, stfl2.created_date, '2000-01-01')";
                        attempt = totalCnt;

                    } else { // not everything
                        question = getDisplayMember("copystflbetweendatabases{updated}",
                            "Do you want load the {0} updated table field language rows from the new release?", newerCnt.ToString());
                        dr = showMessageBox(worker, question, title, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                        if (dr == DialogResult.Yes) {
                            attempt = newCnt + newerCnt;
                            where = "WHERE COALESCE(stfl1.modified_date, stfl1.created_date) > COALESCE(stfl2.modified_date, stfl2.created_date, '2000-01-01')";
                        } else {
                            attempt = newCnt;
                            where = "WHERE stfl2.created_date IS NULL";
                        }
                    }

                    worker.ReportProgress(0, getDisplayMember("copystflbetweendatabases{upgradestfl}", "Upgrading {0} sys table field lang...", attempt.ToString()));

                    string updateSql = @"
If(OBJECT_ID('tempdb..#stfldiff') Is Not Null) Drop Table #stfldiff;

SELECT st1.table_name, stf1.field_name, stfl1.sys_table_field_lang_id AS from_id, stfl2.sys_table_field_lang_id AS to_id
, COALESCE(stfl1.modified_date, stfl1.created_date) AS from_date
, COALESCE(stfl2.modified_date, stfl2.created_date) AS to_date
INTO #stfldiff
FROM ##TPRE1##sys_table st1
JOIN ##TPRE1##sys_table_field stf1 ON stf1.sys_table_id = st1.sys_table_id
JOIN ##TPRE1##sys_table_field_lang stfl1 ON stfl1.sys_table_field_id = stf1.sys_table_field_id
JOIN ##TPRE2##sys_table st2 ON st2.table_name = st1.table_name
JOIN ##TPRE2##sys_table_field stf2 ON stf2.sys_table_id = st2.sys_table_id AND stf2.field_name = stf1.field_name
LEFT JOIN ##TPRE2##sys_table_field_lang stfl2 ON stfl2.sys_table_field_id = stf2.sys_table_field_id AND stfl2.sys_lang_id = stfl1.sys_lang_id
##WHERE##
ORDER BY st1.table_name, stf1.field_name, stfl1.sys_lang_id

-- add new stfl
INSERT INTO ##TPRE2##sys_table_field_lang
(sys_table_field_id, sys_lang_id, title, description, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
SELECT
	(SELECT sys_table_field_id FROM ##TPRE2##sys_table_field stf2 JOIN ##TPRE2##sys_table st2 ON st2.sys_table_id = stf2.sys_table_id
		WHERE stf2.field_name = stf1.field_name AND st2.table_name = st1.table_name
	),
    sys_lang_id, title, description, stfl1.created_date, stfl1.created_by, stfl1.modified_date, stfl1.modified_by, stfl1.owned_date, stfl1.owned_by
FROM #stfldiff
JOIN ##TPRE1##sys_table_field_lang stfl1 ON stfl1.sys_table_field_lang_id = #stfldiff.from_id
JOIN ##TPRE1##sys_table_field stf1 ON stf1.sys_table_field_id = stfl1.sys_table_field_id
JOIN ##TPRE1##sys_table st1 ON st1.sys_table_id = stf1.sys_table_id
WHERE #stfldiff.to_id IS NULL

-- update
UPDATE stfl2 SET
    stfl2.title = stfl1.title,
    stfl2.description = stfl1.description,
    stfl2.created_date = stfl1.created_date,
    stfl2.modified_date = stfl1.modified_date,
    stfl2.modified_by = stfl1.modified_by
FROM #stfldiff
JOIN ##TPRE1##sys_table_field_lang stfl1 ON stfl1.sys_table_field_lang_id = #stfldiff.from_id
JOIN ##TPRE2##sys_table_field_lang stfl2 ON stfl2.sys_table_field_lang_id = #stfldiff.to_id
WHERE #stfldiff.to_id IS NOT NULL

-- Done, clean up temp table
If(OBJECT_ID('tempdb..#stfldiff') Is Not Null) Drop Table #stfldiff;";

                    updateSql = updateSql.Replace("##TPRE1##", fromTablePrefix).Replace("##TPRE2##", toTablePrefix).Replace("##WHERE##", where);
                    dm.Write(updateSql);
                }
            }
        }


        public void CopyDtBetweenDatabases(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, Creator c, string dataDestinationFolder, string targetDir, string superUserPassword, string toTablePrefix, string fromTablePrefix, bool ask) {
            using (DataManager dm = DataManager.Create(c.DataConnectionSpec)) {
                string findSql = @"
SELECT st1.table_name, sdv1.dataview_name, sd1.fully_qualified_class_name
, sd1.sys_datatrigger_id AS from_id, sd2.sys_datatrigger_id AS to_id
, COALESCE(sd1.modified_date, sd1.created_date) AS from_date
, COALESCE(sd2.modified_date, sd2.created_date) AS to_date
FROM ##TPRE1##sys_datatrigger sd1
LEFT JOIN ##TPRE2##sys_datatrigger sd2 ON sd1.fully_qualified_class_name = sd2.fully_qualified_class_name
LEFT JOIN ##TPRE1##sys_table st1 ON st1.sys_table_id = sd1.sys_table_id
LEFT JOIN ##TPRE1##sys_dataview sdv1 ON sdv1.sys_dataview_id = sd1.sys_dataview_id
ORDER BY st1.table_name, sd1.fully_qualified_class_name";
                findSql = findSql.Replace("##TPRE1##", fromTablePrefix).Replace("##TPRE2##", toTablePrefix);

                DataTable dt = dm.Read(findSql);
                int totalCnt = dt.Rows.Count;
                int newCnt = dt.Select("to_date IS NULL").Length;
                int newerCnt = dt.Select("to_date < from_date").Length;
                int attempt = 0;
                string where = null;

                if (totalCnt > 0) {
                    string question = getDisplayMember("copyDt{updtbody}",
                        @"Do you want load all {0} datatriggers from the new release?
({1} are new and {2} are updated.)

    No will load just new and/or updated, Cancel will load none."
                        , totalCnt.ToString(), newCnt.ToString(), newerCnt.ToString());
                    string title = getDisplayMember("copyDt{updttitle}", "Datatrigger Update");

                    DialogResult dr = DialogResult.Yes;
                    if (ask) dr = showMessageBox(worker, question, title, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                    if (dr == DialogResult.Cancel) {
                        worker.ReportProgress(0, getDisplayMember("copyDt{skipdtupgrade}", "Skipping datatrigger upgrade..."));
                        return;
                    } else if (dr == DialogResult.Yes) {
                        where = null;
                        attempt = totalCnt;

                    } else { //not everything
                        attempt = newCnt;
                        where = "WHERE sd2.created_date IS NULL";
                        if (newerCnt > 0) {
                            question = getDisplayMember("copyDt{dbalreadyexistsbody}",
                                @"As well as the {0} completely new datatrigger(s), do you want to load the {1} more recent datatrigger(s)?

    No will just load the {0} new, Cancel will load none.", newCnt.ToString(), newerCnt.ToString());

                            dr = showMessageBox(worker, question, title, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                            if (dr == DialogResult.Cancel) {
                                worker.ReportProgress(0, getDisplayMember("copyDt{skipdtupgrade}", "Skipping datatrigger upgrade..."));
                                return;
                            } else if(dr == DialogResult.Yes) {
                                attempt = newCnt + newerCnt;
                                where = "WHERE COALESCE(sd1.modified_date, sd1.created_date) > COALESCE(sd2.modified_date, sd2.created_date, '2000-01-01')";
                            }
                        }
                    }

                    worker.ReportProgress(0, getDisplayMember("upgradeDatabaseInstall{upgradedvs}", "Upgrading {0} datatriggers...", attempt.ToString()));

                    string updateSql = @"
If(OBJECT_ID('tempdb..#dtdiff') Is Not Null) Drop Table #dtdiff;

SELECT st1.table_name, sdv1.dataview_name, sd1.fully_qualified_class_name
, sd1.sys_datatrigger_id AS from_id, sd2.sys_datatrigger_id AS to_id
, COALESCE(sd1.modified_date, sd1.created_date) AS from_date
, COALESCE(sd2.modified_date, sd2.created_date) AS to_date
INTO #dtdiff
FROM ##TPRE1##sys_datatrigger sd1
LEFT JOIN ##TPRE2##sys_datatrigger sd2 ON sd1.fully_qualified_class_name = sd2.fully_qualified_class_name
LEFT JOIN ##TPRE1##sys_table st1 ON st1.sys_table_id = sd1.sys_table_id
LEFT JOIN ##TPRE1##sys_dataview sdv1 ON sdv1.sys_dataview_id = sd1.sys_dataview_id
##WHERE##
ORDER BY st1.table_name, sd1.fully_qualified_class_name

-- add new datatrigger rows
INSERT INTO ##TPRE2##sys_datatrigger
    (sys_dataview_id, sys_table_id, virtual_file_path, assembly_name, fully_qualified_class_name, is_enabled, is_system, sort_order,
    created_date, created_by, modified_date, modified_by, owned_date, owned_by)
SELECT
    (SELECT sys_dataview_id FROM sys_dataview WHERE dataview_name = #dtdiff.dataview_name),
    (SELECT sys_table_id FROM sys_table WHERE table_name = #dtdiff.table_name),
    virtual_file_path, assembly_name, sd1.fully_qualified_class_name, is_enabled, is_system, sort_order,
    created_date, created_by, modified_date, modified_by, owned_date, owned_by
FROM #dtdiff
JOIN ##TPRE1##sys_datatrigger sd1 ON sd1.sys_datatrigger_id = #dtdiff.from_id
WHERE #dtdiff.to_id IS NULL

-- update matching datatrigger rows
UPDATE sd2 SET
    sd2.sys_dataview_id = (SELECT sys_dataview_id FROM sys_dataview WHERE dataview_name = #dtdiff.dataview_name),
    sd2.sys_table_id = (SELECT sys_table_id FROM sys_table WHERE table_name = #dtdiff.table_name),
    sd2.virtual_file_path = sd1.virtual_file_path,
    sd2.assembly_name = sd1.assembly_name,
    sd2.fully_qualified_class_name = sd1.fully_qualified_class_name,
    sd2.is_enabled = sd1.is_enabled,
    sd2.is_system = sd1.is_system,
    sd2.sort_order = sd1.sort_order,
    sd2.created_date = sd1.created_date,
    sd2.modified_date = sd1.modified_date,
    sd2.modified_by = sd1.modified_by
FROM #dtdiff
JOIN ##TPRE1##sys_datatrigger sd1 ON sd1.sys_datatrigger_id = #dtdiff.from_id
JOIN ##TPRE2##sys_datatrigger sd2 ON sd2.sys_datatrigger_id = #dtdiff.to_id
WHERE #dtdiff.to_id IS NOT NULL

-- Done, clean up temp table
If(OBJECT_ID('tempdb..#dtdiff') Is Not Null) Drop Table #dtdiff;";

                    updateSql = updateSql.Replace("##TPRE1##", fromTablePrefix).Replace("##TPRE2##", toTablePrefix).Replace("##WHERE##", where);
                    dm.Write(updateSql);
                }
            }
        }


        public void CopyCvlBetweenDatabases(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, Creator c, string dataDestinationFolder, string targetDir, string superUserPassword, string toTablePrefix, string fromTablePrefix, bool ask) {
            using (DataManager dm = DataManager.Create(c.DataConnectionSpec)) {
                string findSql = @"
SELECT cv1.group_name, cv1.value, cvl1.sys_lang_id, cvl1.title, cvl1.description --, cvl2.title, cvl2.description
	, cv1.code_value_id AS from_cv_id, cv2.code_value_id AS to_cv_id
	, cvl1.code_value_lang_id AS from_cvl_id, cvl2.code_value_lang_id AS to_cvl_id
	, COALESCE(cvl1.modified_date, cvl1.created_date) AS from_date
	, COALESCE(cvl2.modified_date, cvl2.created_date) AS to_date
FROM ##TPRE1##code_value_lang cvl1
JOIN ##TPRE1##code_value cv1 ON cv1.code_value_id = cvl1.code_value_id
LEFT JOIN ##TPRE2##code_value cv2 ON cv2.group_name = cv1.group_name AND cv2.value = cv1.value
LEFT JOIN ##TPRE2##code_value_lang cvl2 ON cvl2.code_value_id = cv2.code_value_id AND cvl2.sys_lang_id = cvl1.sys_lang_id
WHERE NOT (COALESCE(cvl1.title,'') = COALESCE(cvl2.title, '') AND COALESCE(cvl1.description,'') = COALESCE(cvl2.description,''))
   OR COALESCE(cvl1.modified_date, cvl1.created_date) > COALESCE(cvl2.modified_date, cvl2.created_date, '2000-01-01')
ORDER BY cv1.group_name, cv1.value, cvl1.sys_lang_id
";
                findSql = findSql.Replace("##TPRE1##", fromTablePrefix).Replace("##TPRE2##", toTablePrefix);

                DataTable dt = dm.Read(findSql);
                int totalCnt = dt.Rows.Count;
                int newCnt = dt.Select("to_date IS NULL").Length;
                int newerCnt = dt.Select("to_date < from_date").Length;
                int attempt = 0;
                string where = null;

                if (totalCnt > 0) {
                    string question = getDisplayMember("copycvlbetweendatabases{upcvlbody}",
                        @"Do you want load all {0} modified code_value_lang rows from the new release?
                        ({1} are new and {2} are updated.)

    No will load just new and updated, Cancel will load none."
                        , totalCnt.ToString(), newCnt.ToString(), newerCnt.ToString());
                    string title = getDisplayMember("copycvlbetweendatabases{upcvltitle}", "Code Value Lang Update");
                    DialogResult dr = DialogResult.Yes;
                    if (ask) dr = showMessageBox(worker, question, title, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                    if (dr == DialogResult.Cancel) {
                        worker.ReportProgress(0, getDisplayMember("copycvlbetweendatabases{skipcvlupgrade}", "Skipping code_value_lang upgrade..."));
                        return;
                    } else if (dr == DialogResult.Yes) {
                        where = @"WHERE NOT (COALESCE(cvl1.title,'') = COALESCE(cvl2.title, '') AND COALESCE(cvl1.description,'') = COALESCE(cvl2.description,''))
   OR COALESCE(cvl1.modified_date, cvl1.created_date) > COALESCE(cvl2.modified_date, cvl2.created_date, '2000-01-01')";
                        attempt = totalCnt;
                    } else {
                        question = getDisplayMember("copycvlbetweendatabases{updated}",
                            "Do you want load the {0} updated code_value_lang rows from the new release?", newerCnt.ToString());
                        dr = showMessageBox(worker, question, title, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                        if (dr == DialogResult.Yes) {
                            attempt = newCnt + newerCnt;
                            where = "WHERE COALESCE(cvl1.modified_date, cvl1.created_date) > COALESCE(cvl2.modified_date, cvl2.created_date, '2000-01-01')";
                        } else {
                            attempt = newCnt;
                            where = "WHERE cvl2.code_value_id IS NULL";
                        }
                    }

                    worker.ReportProgress(0, getDisplayMember("copycvlbetweendatabases{upgradedvs}", "Upgrading {0} code value lang...", attempt.ToString()));

                    string updateSql = @"
If(OBJECT_ID('tempdb..#cvldiff') Is Not Null) Drop Table #cvldiff;

SELECT cv1.group_name, cv1.value-- , cvl1.sys_lang_id, cvl1.title, cvl1.description, cvl2.title, cvl2.description
	, cv1.code_value_id AS from_cv_id, cv2.code_value_id AS to_cv_id
	, cvl1.code_value_lang_id AS from_cvl_id, cvl2.code_value_lang_id AS to_cvl_id
	, COALESCE(cvl1.modified_date, cvl1.created_date) AS from_date
	, COALESCE(cvl2.modified_date, cvl2.created_date) AS to_date
INTO #cvldiff
FROM ##TPRE1##code_value_lang cvl1
JOIN ##TPRE1##code_value cv1 ON cv1.code_value_id = cvl1.code_value_id
LEFT JOIN ##TPRE2##code_value cv2 ON cv2.group_name = cv1.group_name AND cv2.value = cv1.value
LEFT JOIN ##TPRE2##code_value_lang cvl2 ON cvl2.code_value_id = cv2.code_value_id AND cvl2.sys_lang_id = cvl1.sys_lang_id
##WHERE##
ORDER BY cv1.group_name, cv1.value, cvl1.sys_lang_id

-- add new code value rows
INSERT INTO ##TPRE2##code_value
    (group_name, value, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
SELECT group_name, value, created_date, created_by, modified_date, modified_by, owned_date, owned_by
FROM ##TPRE1##code_value
WHERE code_value_id IN (SELECT DISTINCT from_cv_id FROM #cvldiff WHERE to_cv_id IS NULL)

-- add new code value lang rows
INSERT INTO ##TPRE2##code_value_lang
    (code_value_id, sys_lang_id, title, description, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
SELECT 
    (SELECT code_value_id FROM ##TPRE2##code_value WHERE group_name = #cvldiff.group_name AND value = #cvldiff.value),
    sys_lang_id, title, description, created_date, created_by, modified_date, modified_by, owned_date, owned_by
FROM ##TPRE1##code_value_lang cvl1
JOIN #cvldiff ON #cvldiff.from_cvl_id = cvl1.code_value_lang_id
WHERE #cvldiff.to_cvl_id IS NULL

-- update matching code value lang rows

UPDATE cvl2 SET
    cvl2.title  = cvl1.title, 
    cvl2.description    = cvl1.description,
    cvl2.created_date   = cvl1.created_date,
    cvl2.created_by     = cvl1.created_by,
    cvl2.modified_date  = cvl1.modified_date,
    cvl2.modified_by    = cvl2.modified_by
FROM #cvldiff
JOIN ##TPRE1##code_value_lang cvl1 ON cvl1.code_value_lang_id = #cvldiff.from_cvl_id
JOIN ##TPRE2##code_value_lang cvl2 ON cvl2.code_value_lang_id = #cvldiff.to_cvl_id
WHERE #cvldiff.to_cvl_id IS NOT NULL 

-- Done, clean up temp table
If(OBJECT_ID('tempdb..#cvldiff') Is Not Null) Drop Table #cvldiff;";

                    updateSql = updateSql.Replace("##TPRE1##", fromTablePrefix).Replace("##TPRE2##", toTablePrefix).Replace("##WHERE##", where);
                    dm.Write(updateSql);
                }
            }
        }


        private void populateDatabase(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, ref Creator c, ref List<TableInfo> tables, string dataDestinationFolder, string targetDir, string superUserPassword, string databaseName, string fromDbName) {

            sb.AppendLine("Getting instance of creator");
            // read schema info from the unzipped files

            if (dbEngineUtil is OracleEngineUtil) {
                // The built-in oracleconnection class in .NET uses OCI (Oracle Callable Interface) to do the actual work.
                // OCI does not allow logins to use AS SYSDBA or AS SYSOPER.
                // Since all the work we're doing will be within the gringlobal user's table space, we can login as them and do it instead.
                // (we initially create the database using the sql_plus.exe itself, which does support AS SYSDBA or AS SYSOPER)
                c = Creator.GetInstance(dbEngineUtil.GetDataConnectionSpec(databaseName, databaseName, "TempPA55"));
            } else {
                c = Creator.GetInstance(dbEngineUtil.GetDataConnectionSpec(databaseName, dbEngineUtil.SuperUserName, superUserPassword));
            }
            c.Worker = worker;
            c.OnProgress += new ProgressEventHandler(c_OnProgress);

            sb.AppendLine("Reading schema information");
            worker.ReportProgress(0, getDisplayMember("populateDatabase{readingschema}", "Reading schema information..."));
            // __schema.xml is in the installation path...
            tables = c.LoadTableInfo(dataDestinationFolder);

            sb.AppendLine("Creating tables");
            worker.ReportProgress(0, getDisplayMember("populateDatabase{creatingtables}", "Creating tables..."));

       //     showMessageBox(worker, c.DataConnectionSpec.ToString(), "Connection Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            c.CreateTables(tables, databaseName);
            sb.AppendLine("Loading data");
            worker.ReportProgress(0, getDisplayMember("populateDatabase{loadingdata}", "Loading data..."));
            // actual data files are in the data destination folder created when we extracted the cab files...
            c.CopyDataToDatabase(dataDestinationFolder, databaseName, tables, false, false);

            // Copy user data from another local database if specified
            if (!string.IsNullOrEmpty(fromDbName)) {
                // decide which tables to copy
                foreach (TableInfo ti in tables) {
                    if (ti.TableName.StartsWith("sys_")) {
                        ti.IsSelected = false;
                        if (ti.TableName == "sys_user") ti.IsSelected = true;
                    } else {
                        ti.IsSelected = true;
                        if (ti.TableName.StartsWith("code_value") || ti.TableName == "app_setting" || ti.TableName == "app_resource") ti.IsSelected = false;
                        
                    }
                }
                worker.ReportProgress(0, getDisplayMember("populateDatabase{copyingdata}", "Copying data from {0}, this may take some time...", fromDbName));
                string result;
                result = c.CopyDataBetweenDatabases(databaseName, fromDbName, tables);
                var form = new frmPreview();
                form.txtPreview.Text = result;
                form.ShowDialog();
            }

            // create users / assign rights
            createDatabaseUsers(worker, sb, dbEngineUtil, c, tables, targetDir, superUserPassword, databaseName);


            //// reload table info, CopyDataToDatabase empties out the collection
            //tables = c.LoadTableInfo(dataDestinationFolder);

            sb.AppendLine("Creating indexes...");
            worker.ReportProgress(0, getDisplayMember("populateDatabase{creatingindexes}", "Creating indexes..."));
            c.CreateIndexes(tables);


            // 2010-09-18 Brock Weaver brock@circaware.com
            // HACK: since this installer code does not support pulling in portions of a table, we need to make sure the only data that
            //       exists is what the user chose.  This problem only occurs in tables which have two FKs, each of which are in different
            //       cab files (i.e. citation_map point at both taxonomy tables and accession tables, but those are in different cabs. Putting
            //       citation_map in either cab doesn't solve the problem.)
            //       The solution we're going to use is hardcode here those certain tables that straddle cab files and remove records as needed
            //       after the data was imported and indexes created (just above) but before constraints are applied (just after this method call)
            scrubTablesThatPointAtMissingData(worker, dataDestinationFolder, databaseName, c.DataConnectionSpec);


            sb.AppendLine("Creating foreign key constraints");
            worker.ReportProgress(0, getDisplayMember("populateDatabase{creatingforeignkeys}", "Creating foreign key constraints..."));
            c.CreateConstraints(tables, true, true);
        }

        private void scrubTablesThatPointAtMissingData(BackgroundWorker worker, string dataDestinationFolder, string databaseName, DataConnectionSpec dcs){


            // delete data from tables that straddle cab files that reference data the user chose not to install.  
            // case in point: the citation_map table...
            //  taxonomy_genus <-- citation_map --> accession

            using (var dm = DataManager.Create(dcs)) {

                var files = Directory.GetFiles(dataDestinationFolder, "*.txt");

//                if (!files.Contains(@"\taxonomy.txt")) {
//                    // user did not install taxonomy data.  remove almost all the cooperators so their base install is less cluttered.
//                    worker.ReportProgress(0, "Removing unnecessary cooperator information...");

//                    // rip out all cooperators that we don't absolutely require.
//                    // NOTE: current_cooperator_id makes this...interesting.
//                    var coopSubselect = @"
//        select coalesce(cooperator_id,-1) from sys_user
//
//        UNION
//        select coalesce(cooperator_id,-1) from cooperator where last_name = 'SYSTEM'
//
//        UNION
//        select coalesce(cooperator_id,-1) from cooperator_map
//
//        UNION
//        select coalesce(created_by,-1) from geography
//        UNION
//        select coalesce(modified_by,-1) from geography
//        UNION
//        select coalesce(owned_by,-1) from geography
//
//        UNION
//        select coalesce(created_by,-1) from geography_lang
//        UNION
//        select coalesce(modified_by,-1) from geography_lang
//        UNION
//        select coalesce(owned_by,-1) from geography_lang
//
//        UNION
//        select coalesce(created_by,-1) from geography_region_map
//        UNION
//        select coalesce(modified_by,-1) from geography_region_map
//        UNION
//        select coalesce(owned_by,-1) from geography_region_map
//
//        UNION
//        select coalesce(created_by,-1) from site
//        UNION
//        select coalesce(modified_by,-1) from site
//        UNION
//        select coalesce(owned_by,-1) from site
//
//        UNION
//        select coalesce(created_by,-1) from region
//        UNION
//        select coalesce(modified_by,-1) from region
//        UNION
//        select coalesce(owned_by,-1) from region
//
//        UNION
//        select coalesce(created_by,-1) from region_lang
//        UNION
//        select coalesce(modified_by,-1) from region_lang
//        UNION
//        select coalesce(owned_by,-1) from region_lang
//
//        UNION
//        select coalesce(created_by,-1) from code_value
//        UNION
//        select coalesce(modified_by,-1) from code_value
//        UNION
//        select coalesce(owned_by,-1) from code_value
//
//        UNION
//        select coalesce(created_by,-1) from code_value_lang
//        UNION
//        select coalesce(modified_by,-1) from code_value_lang
//        UNION
//        select coalesce(owned_by,-1) from code_value_lang
//
//        UNION
//        select coalesce(created_by,-1) from cooperator_group
//        UNION
//        select coalesce(modified_by,-1) from cooperator_group
//        UNION
//        select coalesce(owned_by,-1) from cooperator_group
//
//        UNION
//        select coalesce(created_by,-1) from cooperator_map
//        UNION
//        select coalesce(modified_by,-1) from cooperator_map
//        UNION
//        select coalesce(owned_by,-1) from cooperator_map
//
//";

//                    dm.Write(String.Format(@"
//delete from 
//    cooperator
//where
//    cooperator_id not in ({0})
//    and current_cooperator_id not in ({0})
//", coopSubselect));

//                }

                if (!files.Contains(@"\accession.txt")){
                    // user did not install accession data. remove records that point at accession data.
                    worker.ReportProgress(0, getDisplayMember("scrubTablesThatPointAtMissingData{removingaccession}", "Removing unnecessary accession citation information..."));
//                    dm.Write(@"
//delete from 
//    citation 
//where 
//    (accession_id is not null and accession_id not in (select accession_id from accession))
//    or (accession_ipr_id is not null and accession_ipr_id not in (select accession_ipr_id from accession_ipr))
//    or (accession_pedigree_id is not null and accession_pedigree_id not in (select accession_pedigree_id from accession_pedigree))
//");
                }

                if (!files.Contains(@"\genetic_marker.txt")) {
                    worker.ReportProgress(0, getDisplayMember("scrubTablesThatPointAtMissingData{removinggenetic}", "Removing unnecessary genetic citation information..."));
                    dm.Write(@"delete from citation where genetic_marker_id is not null and genetic_marker_id not in (select genetic_marker_id from genetic_marker)");
                }

                if (!files.Contains(@"\method.txt")) {
                    worker.ReportProgress(0, getDisplayMember("scrubTablesThatPointAtMissingData{removingmethod}", "Removing unnecessary method citation information..."));
                    dm.Write(@"delete from citation where method_id is not null and method_id not in (select method_id from method)");
                }

                if (!files.Contains(@"\order_request.txt")) {
                    // user did not install order data.  remove records that point at order data.
                    worker.ReportProgress(0, getDisplayMember("scrubTablesThatPointAtMissingData{removingaccession_annotation}", "Removing unnecessary accession_inv_annotation citation information..."));
                    dm.Write(@"delete from accession_inv_annotation where order_request_id is not null and order_request_id not in (select order_request_id from order_request)");

                    worker.ReportProgress(0, getDisplayMember("scrubTablesThatPointAtMissingData{removinginventory_viability}", "Removing unnecessary inventory_viability citation information..."));
                    dm.Write(@"delete from inventory_viability_data where order_request_item_id is not null and order_request_item_id not in (select order_request_item_id from order_request_item)");

                }


            }
        }

        private void promptForAndExtractAdditionalData(BackgroundWorker worker, StringBuilder sb, string dataDestinationFolder, string gguacPath) {

            // HACK: try to give MSI form a moment to focus before we show our dialog...
            Thread.Sleep(1000);
            Application.DoEvents();
            Thread.Sleep(500);

            // we may need to prompt user to see what data they want to download from the server and copy into the database
            var fid = new frmInstallData();

            DialogResult result = DialogResult.Cancel;
            var autoIncludeOptionalData = Utility.GetParameter("optionaldata", null, this.Context, null);
            if (("" + autoIncludeOptionalData).ToUpper() == "TRUE" || ("" + autoIncludeOptionalData).ToUpper() == "1") {
                // command line tells us to include optional data, don't prompt for it
                worker.ReportProgress(0, getDisplayMember("promptForAndExtractAdditionalData{autodownloading}", "Auto-downloading optional data..."));
                sb.AppendLine("Auto-downloading optional data...");
                fid.DownloadAllFiles();
                result = DialogResult.OK;
            } else if (("" + autoIncludeOptionalData).ToUpper() == "FALSE" || ("" + autoIncludeOptionalData).ToUpper() == "0"){
                worker.ReportProgress(0, getDisplayMember("promptForAndExtractAdditionalData{autoskipping}", "Auto-skipping download of optional data..."));
                sb.AppendLine("Auto-skipping download of optional data...");
                result = DialogResult.OK;
            } else {
                worker.ReportProgress(0, getDisplayMember("promptForAndExtractAdditionalData{prompting}", "Prompting for optional data..."));
                sb.AppendLine("Prompting for optional data...");
                result = fid.ShowDialog();
            }

            if (result == DialogResult.OK) {
                if (fid.DataFiles == null || fid.DataFiles.Count == 0) {
                    worker.ReportProgress(0, getDisplayMember("promptForAndExtractAdditionalData{nonselected}", "User did not select any optional data files."));
                    sb.AppendLine("User did not select any optional data files.");
                    Application.DoEvents();
                } else {
                    var splash = new frmSplash();
                    try {
                        var extracting = getDisplayMember("promptForAndExtractAdditionalData{extracting}", "Extracting optional data files...");
                        worker.ReportProgress(0, extracting);
                        sb.AppendLine("Extracting optional data files...");
                        splash.Show(extracting, false, null);
                        foreach (var s in fid.DataFiles) {
                            sb.AppendLine("Extracting files from " + s + " to folder " + dataDestinationFolder);
                            // extract each cab file
                            //                        string fn = Path.GetFileNameWithoutExtension(s).Replace("_", " ");

                            worker.ReportProgress(0, getDisplayMember("promptForAndExtractAdditionalData{extractingfile}", "Extracting {0} to {1}...", s, dataDestinationFolder));
                            splash.ChangeText(getDisplayMember("promptForAndExtractAdditionalData{extractingfilesplash}", "Extracting {0}\n\nPlease wait while data is extracted...", s));
                            Application.DoEvents();
                            Utility.ExtractCabFile(s, dataDestinationFolder, gguacPath);

                            // we expanded the cab file, now delete it since we don't need it anymore (and want the next install to re-request data from the server and ignore the cache)
                            try {
                                var moveTo = s.Replace(@"\downloaded\", @"\installed\");
                                if (File.Exists(moveTo)) {
                                    File.Delete(moveTo);
                                }
                                File.Move(s, moveTo);
                            } catch {
                                try {
                                    // move failed, try to delete it
                                    File.Delete(s);
                                } catch {
                                    // ultimately ignore all file movement errors
                                }
                            }
                        }
                    } finally {
                        splash.Close();
                        Application.DoEvents();
                    }
                }
            } else {
                EventLog.WriteEntry("GRIN-Global Database", "User cancelled out of optional data selection dialog", EventLogEntryType.Error);
                //Context.LogMessage("User cancelled out of optional data selection dialog");
                //this.Rollback(e.SavedState);
                throw new InvalidOperationException(getDisplayMember("promptForAndExtractAdditionalData{usercancel}", "User cancelled out of optional data selection dialog."));
            }

            if (_frmProgress != null) {
                Toolkit.ActivateApplication(_frmProgress.Handle);
            }
        }

        private void extractSystemDataFile(BackgroundWorker worker, StringBuilder sb, string targetDir, string gguacPath, string dataDestinationFolder) {

            //                string helperPath = (targetDir + @"\gguac.exe").Replace(@"\\", @"\");
            worker.ReportProgress(0, getDisplayMember("extractSystemDataFile", "Extracting system data file..."));
            sb.AppendLine("Extracting system data file...");
            var sourceCab = (targetDir + @"\system_data.cab").Replace(@"\\", @"\");
            var extractOutput = Utility.ExtractCabFile(sourceCab, dataDestinationFolder, gguacPath);



            // copy schema file to data dest folder
            string tgtXml = (dataDestinationFolder + @"\__schema.xml").Replace(@"\\", @"\");
            if (File.Exists(tgtXml)) {
                File.Delete(tgtXml);
            }
            File.Copy((targetDir + @"\__schema.xml").Replace(@"\\", @"\"), tgtXml);
        }

        private void createDatabase(BackgroundWorker worker, DatabaseEngineUtil dbEngineUtil, string targetDir, string superUserPassword, string databaseName) {
            // special case:
            // this is the first time we try to login to the database after the db engine is installed.
            // if we get user login problems, prompt them whether to bail or continue after they've taken some action.

            bool done = false;
            string output = null;
            while (!done) {
                try {
                  done = true;

                    output = dbEngineUtil.CreateDatabase(superUserPassword, databaseName);

                } catch (Exception exCreate) {

                    if (exCreate.Message.ToLower().Contains("already exists. choose a different database name.")) {
                        // catch above
                        throw;
                    } else if (exCreate.Message.ToLower().Contains("local security authority cannot be contacted")) {
                        // special case: using windows login for sql server.
                        //               computer is part of a domain
                        //               computer can't authenticate with domain (i.e. laptop not VPN'd into network with the necessary domain controller)
                        // http://sqldbpool.wordpress.com/2008/09/05/%E2%80%9Ccannot-generate-sspi-context%E2%80%9D-error-message-more-comments-for-sql-server/
                        // http://blogs.msdn.com/sql_protocols/archive/2005/10/19/482782.aspx
                        EventLog.WriteEntry("GRIN-Global Database", "Exception on first pass of creating database: " + exCreate.Message, EventLogEntryType.Error);
                        var dr = showMessageBox(worker, getDisplayMember("createDatabase{windowsauthfailedbody}", "Windows login could not be authenticated.\nIf your computer is part of a Windows domain, please do either one of the following and try again:\n\nA) VPN into the network that hosts your Windows domain\nB) Disconnect from all networks\n\nAlso, you can try enabling Mixed Mode in SQL Server and logging in with the SQL Server 'sa' account.\n\nDo you want to try again?"), getDisplayMember("createDatabase{windowsauthfailedtitle}", "Could not verify login to an authority"), MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (dr == DialogResult.Yes) {
                            done = false;
                        }

                    }

                    if (done) {
                        //Context.LogMessage("Exception creating database: " + exCreate.Message);
                        EventLog.WriteEntry("GRIN-Global Database", "Exception creating database: " + exCreate.Message, EventLogEntryType.Error);
                        showMessageBox(worker, getDisplayMember("createDatabase{exceptionbody}", "Exception creating database: {0}", exCreate.Message), getDisplayMember("createDatabase{exceptiontitle}", "Failed Creating Database"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //this.Rollback(e.SavedState);
                        throw;
                    }
                }
            }

        }

        private string createDependentDatabaseObjects(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, Creator c, List<TableInfo> tables, string targetDir, string superUserPassword, string databaseName) {

            if (dbEngineUtil is MySqlEngineUtil){

                // MySQL has nothing to do here
            } else if (dbEngineUtil is SqlServerEngineUtil){

                // SQL Server has nothing to do here

            } else if (dbEngineUtil is OracleEngineUtil) {

                // Oracle needs to create triggers and sequences...

                // get all the sequences up to snuff so new inserts work properly (i.e. make sure sequence id's are past the last one currently in each table)
                worker.ReportProgress(0, getDisplayMember("createDependentDatabaseObjects{sync}", "Synchronizing sequences with table data..."));

                var oraUtil = dbEngineUtil as OracleEngineUtil;

                foreach (var t in tables) {
                    worker.ReportProgress(0, getDisplayMember("createDependentDatabaseObjects{synctable}", "Synchronizing sequence for table {0}...", t.TableName));
                    var ret = oraUtil.RestartSequenceForTable(superUserPassword, databaseName, "TempPA55", t.TableName, t.PrimaryKey.Name);
                }
            } else if (dbEngineUtil is PostgreSqlEngineUtil) {

                // PostgreSQL needs to create triggers and sequences...
                
                // get all the sequences up to snuff so new inserts work properly (i.e. make sure sequence id's are past the last one currently in each table)
                worker.ReportProgress(0, getDisplayMember("createDependentDatabaseObjects{sync}", "Synchronizing sequences with table data..."));

                var pgUtil = dbEngineUtil as PostgreSqlEngineUtil;

                foreach (var t in tables) {
                    worker.ReportProgress(0, getDisplayMember("createDependentDatabaseObjects{synctable}", "Synchronizing sequence for table {0}...", t.TableName));
                    pgUtil.RestartSequenceForTable(superUserPassword, databaseName, null, t.TableName, t.PrimaryKey.Name);
                }
            }

            return "";
        }

        private string createDatabaseUsers(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, Creator c, List<TableInfo> tables, string targetDir, string superUserPassword, string databaseName) {
            // execute sql to create users

            worker.ReportProgress(0, getDisplayMember("createDatabaseUsers{create}", "Creating new gringlobal users..."));
            sb.AppendLine("Creating users");
            string output = null;
           //KMK 10/5/17  Moved the try to better handle errors
                //KMK 09/08/17  These passwords do not meet password policy
                //string userPassword = "gg_user_PA55w0rd!";
                //string searchPassword = "gg_search_PA55w0rd!";
                string userPassword = "gguPA55w0rd!!11";
                string searchPassword = "ggsPA55w0rd!!11";
                var machineName = Toolkit.Cut(Dns.GetHostName(), 0, 15);
            if (dbEngineUtil is SqlServerEngineUtil)
                {
                    worker.ReportProgress(0, getDisplayMember("createDatabaseUsers{grant}", "Granting rights to all gringlobal tables for users..."));
                    sb.AppendLine("Granting rights");

                #region outdated code
                // KMK 04/20/17  None ofthe below will work with anything past Windows 7. 
                // SQL Server, Integrated authority -- XP
                //output += dbEngineUtil.CreateUser(superUserPassword, "gringlobal", "NETWORK SERVICE", machineName, null, false, true);
                //output += dbEngineUtil.CreateUser(superUserPassword, "gringlobal", "ASPNET", machineName, null, false, true);

                //////// SQL Server, Integrated authority -- Vista
                //output += dbEngineUtil.CreateUser(superUserPassword, "gringlobal", "NETWORK SERVICE", "NT AUTHORITY", null, false, true);
                //    output += dbEngineUtil.CreateUser(superUserPassword, "gringlobal", "SYSTEM", "NT AUTHORITY", null, false, true);

                // SQL Server, Integrated authority -- Windows 7
                //       output += dbEngineUtil.CreateUser(superUserPassword, "gringlobal", "DefaultAppPool", "IIS APPPOOL", null, false, true);
                #endregion
                try
                {
                    //     mixed mode support
                    //KMK 10/4/17  Sometimes, if the db is not uninstalled properly, it will bomb on creating users, thinking they already exists
                    //because sometimes it doesn't actually remove them before the install.  
                    //Changed the code so that if either user does exist, just ignore the error, since we need the users anyway.
                    //Did this only for SQLServer
                    output += dbEngineUtil.CreateUser(superUserPassword, databaseName, "gg_user", machineName, userPassword, false, false);
                    
                }
                catch (Exception ex)
                {
                    if (!ex.ToString().Contains("already exists"))
                    {
                        EventLog.WriteEntry("GRIN-Global Database", "Exception creating users: " + ex.Message, EventLogEntryType.Error);
                        showMessageBox(worker, getDisplayMember("createDatabaseUsers{exceptionbody}", "Exception creating users: {0}", ex.Message), getDisplayMember("createDatabaseUsers{exceptiontitle}", "Creating Users Failed"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                        throw;
                    }

                }
                try {
                    output += dbEngineUtil.CreateUser(superUserPassword, databaseName, "gg_search", machineName, searchPassword, true, false);
                }
                catch (Exception ex)
                {
                    if (!ex.ToString().Contains("already exists"))
                    {
                        EventLog.WriteEntry("GRIN-Global Database", "Exception creating users: " + ex.Message, EventLogEntryType.Error);
                        showMessageBox(worker, getDisplayMember("createDatabaseUsers{exceptionbody}", "Exception creating users: {0}", ex.Message), getDisplayMember("createDatabaseUsers{exceptiontitle}", "Creating Users Failed"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                        throw;
                    }

                }
                return output;

            }

            try { 
             if (dbEngineUtil is MySqlEngineUtil) {            
                    // MySQL 
                    worker.ReportProgress(0, getDisplayMember("createDatabaseUsers{grant}", "Granting rights to all gringlobal tables for users..."));
                    sb.AppendLine("Granting rights");
                    output += dbEngineUtil.CreateUser(superUserPassword, databaseName, "gg_user", "%", userPassword, false, false);
                    output += dbEngineUtil.CreateUser(superUserPassword, databaseName, "gg_search", "%", searchPassword, true, false);

                } else if (dbEngineUtil is OracleEngineUtil) {


                    var res = dbEngineUtil.CreateUser(superUserPassword, databaseName, "gg_user", null, userPassword, false, false);
                    output += res;
                    //showMessageBox(worker, res, "Create user gg_user result", MessageBoxButtons.OK, MessageBoxIcon.None);


                    res = dbEngineUtil.CreateUser(superUserPassword, databaseName, "gg_search", null, searchPassword, true, false);
                    output += res;
                    //showMessageBox(worker, res, "Create user gg_search result", MessageBoxButtons.OK, MessageBoxIcon.None);


                    // also oracle requires us to enumerate each table to grant rights...
                    sb.AppendLine("Granting rights");
                    foreach (var ti in tables) {
                        worker.ReportProgress(0, getDisplayMember("createDatabaseUsers{granttable}", "Granting rights to {0} for necessary users...", ti.TableName));
                        //dbEngineUtil.GrantRightsToTable(superUserPassword, "gringlobal", "gringlobal", null, ti.TableName, false);
                        var grtt = dbEngineUtil.GrantRightsToTable(superUserPassword, databaseName, "gg_user", null, ti.TableName, false);
                        output += grtt;
                        grtt = dbEngineUtil.GrantRightsToTable(superUserPassword, databaseName, "gg_search", null, ti.TableName, true);
                        output += grtt;
                    }
                } else if (dbEngineUtil is PostgreSqlEngineUtil) {

                    // PostgreSQL
                    output += dbEngineUtil.CreateUser(superUserPassword, null, "gg_user", "localhost", userPassword, false, false);
                    output += dbEngineUtil.CreateUser(superUserPassword, null, "gg_search", "localhost", searchPassword, true, false);

                    // postgresql requires us to enumerate each table to grant rights...
                    sb.AppendLine("Granting rights");
                    foreach (var ti in tables) {
                        worker.ReportProgress(0, getDisplayMember("createDatabaseUsers{grantrightstable}", "Granting rights to {0} for necessary users...", ti.TableName));
                        
                        dbEngineUtil.GrantRightsToTable(superUserPassword, databaseName, "gg_user", null, ti.TableName, false);
                        dbEngineUtil.GrantRightsToTable(superUserPassword, databaseName, "gg_search", null, ti.TableName, true);

                    }
                } else if (dbEngineUtil is SqliteEngineUtil){
                    // sqlite has no concept of users, just ignore...
                } else {
                    throw new NotImplementedException(getDisplayMember("createDatabaseUsers{notimplemented}", null, "createDatabaseUsers is not implemented for {0}", dbEngineUtil.EngineName));
                }

            } catch (Exception exCreateUsers) {
                //Context.LogMessage("Exception creating users: " + exCreateUsers.Message);
                EventLog.WriteEntry("GRIN-Global Database", "Exception creating users: " + exCreateUsers.Message, EventLogEntryType.Error);
                showMessageBox(worker, getDisplayMember("createDatabaseUsers{exceptionbody}", "Exception creating users: {0}", exCreateUsers.Message), getDisplayMember("createDatabaseUsers{exceptiontitle}", "Creating Users Failed"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                throw;
            }


            return output;
        }

        private void restartDatabaseEngine(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, string engine, string instanceName, bool windowsAuth, string superUserPassword, string databaseName, InstallEventArgs e) {
            bool restartDone = false;
            while (!restartDone) {
                if (dbEngineUtil == null) {
                    dbEngineUtil = DatabaseEngineUtil.CreateInstance(engine, Utility.GetTargetDirectory(Context, e.SavedState, "Database") + @"\gguac.exe", instanceName);
                    dbEngineUtil.UseWindowsAuthentication = windowsAuth;
                }

                if (dbEngineUtil is SqlServerEngineUtil) {
                    var sqlserverUtil = (dbEngineUtil as SqlServerEngineUtil);
                    sqlserverUtil.InstanceName = instanceName;
                    sqlserverUtil.RefreshProperties();
                }
                var conn = dbEngineUtil.GetDataConnectionSpec(databaseName, dbEngineUtil.SuperUserName, superUserPassword).ConnectionString;
                var maskedPasswordConnString = (String.IsNullOrEmpty(superUserPassword) ? conn : conn.Replace(superUserPassword, "*".PadLeft(superUserPassword.Length, '*')));
                worker.ReportProgress(0, getDisplayMember("restartDatabaseEngine", "Attempting to restart database engine (service name={0}, connectionstring={1}) to close all existing connections...", dbEngineUtil.ServiceName, maskedPasswordConnString));
                try {
                    dbEngineUtil.StopService();
                    dbEngineUtil.StartService();
                    restartDone = true;
                } catch (Exception exReboot) {
                    //Context.LogMessage("Could not restart db engine before dropping databases: " + exReboot.Message);
                    EventLog.WriteEntry("GRIN-Global Database", "Could not restart db engine before dropping databases: " + exReboot.Message, EventLogEntryType.Error);
                    if (DialogResult.Yes != showMessageBox(worker, getDisplayMember("restartDatabaseEngine{exceptionbody}",  "Unable to restart the database engine. Error:\n{0}\n\nInstallation cannot proceed until the database engine is restarted.\nDo you want to try again?", exReboot.Message), getDisplayMember("restartDatabaseEngine{exceptiontitle}", "Restart Database Engine Again?"), MessageBoxButtons.YesNo, MessageBoxIcon.Question)) {
                        restartDone = true;
                        throw;
                    } else {
                        worker.ReportProgress(0, getDisplayMember("restartDatabaseEngine", "Attempting to restart database engine (service name={0}) to close all existing connections...", dbEngineUtil.ServiceName));
                    }
                }
            }

        }

        private string dropDatabase(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, string targetDir, string superUserPassword, string databaseName) {

            string output = "";

            try
            {
                worker.ReportProgress(0, getDisplayMember("dropDatabase{start}", "Dropping any existing {0} databases...",databaseName));
                sb.AppendLine("Dropping databases");
               

                if (dbEngineUtil is SqlServerEngineUtil) {
                    //string lastDB = "gringlobal";
                    try {
                        worker.ReportProgress(0, getDisplayMember("dropDatabase{dropping}", "Dropping database '{0}'...", databaseName));
                        dbEngineUtil.DropDatabase(superUserPassword, databaseName);
                    } catch (Exception exDB) {
                        EventLog.WriteEntry("GRIN-Global Database", "Error while trying to drop database '" + databaseName + "': " + exDB.Message, EventLogEntryType.Error);
                    }

                    // Clean any lingering mdf and ldf files by renaming them
                    string dataDir = dbEngineUtil.DataDirectory;
                    if (Directory.Exists(dataDir)) {
                        string[] files = Directory.GetFiles(dataDir);
                        worker.ReportProgress(0, getDisplayMember("dropDatabase{scan}", "Scanning data directory={0} for existing gringlobal database files, found {1} to inspect...", dataDir, files.Length.ToString()));
                        foreach (string f in files) {
                            try {
                                if (f.ToLower().EndsWith("\\" + databaseName + ".mdf") || f.ToLower().EndsWith("\\" + databaseName + "_log.ldf")) {
                                    // if the drop database failed (e.g. db was not attached), we still want to rename it so our next install goes smoothly
                                    worker.ReportProgress(0, getDisplayMember("dropDatabase{rename}", "Renaming old database file '{0}'...", f));
                                    File.Move(f, f + "_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".ggbackup");
                                }
                            } catch {
                                // eat all errors during a drop or file move
                            }
                        }
                    }
                } else if (dbEngineUtil is OracleEngineUtil)
                {

                    // Oracle user == our concept of database.
                    // drop 'GRINGLOBAL' user to get rid of database.

                    dbEngineUtil.DropDatabase(superUserPassword, databaseName);

                }
                else
                {
                    // MySQL / PostgreSQL / sqlite
                    try
                    {
                        worker.ReportProgress(0, getDisplayMember("dropDatabase{dropping}", "Dropping database {0}...", databaseName));
                        output = dbEngineUtil.DropDatabase(superUserPassword, databaseName);
                    }
                    catch
                    {
                        // eat all errors during a database drop
                    }
                }

            }
            catch (Exception ex)
            {
                // ignore all exceptions when dropping databases
                Debug.Write(ex.Message);
                //Context.LogMessage("Exception dropping database: " + ex.Message);
                EventLog.WriteEntry("GRIN-Global Database", "Exception dropping database: " + ex.Message, EventLogEntryType.Error);
                worker.ReportProgress(0, "Failed to drop database: " + ex.Message);
                //waitForUserInput();
            }
            finally
            {
                worker.ReportProgress(0, getDisplayMember("dropDatabase{completed}", "Dropping database(s) completed."));
            }

            return output;
        }
        //KMK 10/6/17  When creating a database, it may not be available immediately, so this is done 
        //in order to make sure it can be written to
        public string CheckDBAvailability(string databaseName, string superUserPassword, DatabaseEngineUtil dbEngineUtil)
        {
            string available = null;
            if (dbEngineUtil is SqlServerEngineUtil) { 
                
            for (int i = 0; i < 10; i++)
            {
                  
                    //wait before trying, to give it time to become available
                    Thread.Sleep(1000);
                    available = dbEngineUtil.ExecuteSql(superUserPassword, null, "SELECT DATABASEPROPERTYEX('"+databaseName+"', 'Collation')");             
                    if (available != null) { break; }
                   
            }
           }
            return available;
        }

        private void dropDatabaseUsers(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, string superUserPassword, string databaseName) {

            worker.ReportProgress(0, getDisplayMember("dropDatabaseUsers{start}", "Dropping any existing gringlobal users..."));
            sb.AppendLine("Dropping users");

            try
            {

                var machineName = Toolkit.Cut(Dns.GetHostName(), 0, 15);

                if (dbEngineUtil is SqlServerEngineUtil)
                {
                    if (dbEngineUtil.UseWindowsAuthentication)
                    {
                        //// SQL Server, Integrated authority -- XP
                        //dbEngineUtil.DropUser(superUserPassword, "NETWORK SERVICE", machineName);
                        //dbEngineUtil.DropUser(superUserPassword, "ASPNET", machineName);

                        //// SQL Server, Integrated authority -- Vista
                        //dbEngineUtil.DropUser(superUserPassword, "NETWORK SERVICE", "NT AUTHORITY");
                        //// SQL Server, Integrated authority -- Windows 7
                        //dbEngineUtil.DropUser(superUserPassword, "DEFAULTAPPPOOL", "IIS AppPool");
                    }
                    else
                    {
                        // SQL Server mixed mode
                        //KMK 10/4/17  The only flaw with the original code is that if it doesn't acutally drop the user, it just keeps
                        //going and when it tries to create the user, if on an install, it blows up and won't let you continue.
                        //Added the try catch on each 
                        try
                        {
                            dbEngineUtil.DropUser(superUserPassword, databaseName, "gg_user", "localhost");
                            worker.ReportProgress(0, getDisplayMember("dropDatabaseUsers{gg_user}", "Dropped user gg_user"));
                        }
                        catch
                        {
                        }
                        try
                        {
                            dbEngineUtil.DropUser(superUserPassword, databaseName, "gg_search", "localhost");
                            worker.ReportProgress(0, getDisplayMember("dropDatabaseUsers{gg_search}", "Dropped user gg_search"));
                        }
                        catch
                        {
                        }
                    }

                }
                else if (dbEngineUtil is MySqlEngineUtil)
                {
                    // MySQL 
                    try
                    {
                        dbEngineUtil.DropUser(superUserPassword, databaseName, "gg_user", "%");
                        worker.ReportProgress(0, getDisplayMember("dropDatabaseUsers{gg_user}", "Dropped user gg_user"));
                    }
                    catch
                    {
                    }
                    try
                    {
                        dbEngineUtil.DropUser(superUserPassword, databaseName, "gg_search", "%");
                        worker.ReportProgress(0, getDisplayMember("dropDatabaseUsers{gg_user}", "Dropped user gg_search"));
                    }
                    catch
                    {
                    }
                }
                else if (dbEngineUtil is OracleEngineUtil)
                {
                    try
                    {
                        dbEngineUtil.DropUser(superUserPassword, databaseName, "gg_user", "localhost");
                        worker.ReportProgress(0, getDisplayMember("dropDatabaseUsers{gg_user}", "Dropped user gg_user"));
                    }
                    catch
                    {
                    }
                    try
                    {
                        dbEngineUtil.DropUser(superUserPassword, databaseName, "gg_search", "localhost");
                        worker.ReportProgress(0, getDisplayMember("dropDatabaseUsers{gg_user}", "Dropped user gg_search"));
                    }
                    catch
                    {
                    }

                }
                else if (dbEngineUtil is SqliteEngineUtil)
                {
                    // sqlite knows nothing of users
                }
                else if (dbEngineUtil is PostgreSqlEngineUtil)
                {
                    // PostgreSQL
                    try
                    {
                        dbEngineUtil.DropUser(superUserPassword, "postgres", "gg_user", "localhost");
                        worker.ReportProgress(0, getDisplayMember("dropDatabaseUsers{gg_user}", "Dropped user gg_user"));
                    }
                    catch
                    {
                    }
                    try
                    {
                        dbEngineUtil.DropUser(superUserPassword, "postgres", "gg_search", "localhost");
                        worker.ReportProgress(0, getDisplayMember("dropDatabaseUsers{gg_user}", "Dropped user gg_search"));
                    }
                    catch
                    {
                    }
                }
                else
                {
                    throw new NotImplementedException(getDisplayMember("dropDatabaseUsers{notimplemented}", "Dropping users for {0} is not implemented in DatabaseInstaller.dropDatabaseUsers", dbEngineUtil.EngineName));
                }

            }
            catch (Exception exDropUsers)
            {
                Debug.WriteLine(exDropUsers.Message);
                //Context.LogMessage("Exception dropping users: " + exDropUsers.Message);
                //EventLog.WriteEntry("GRIN-Global Database", "Exception dropping users: " + exDropUsers.Message, EventLogEntryType.Error);
                // worker.ReportProgress(0, exDropUsers);
                // waitForUserInput();
            }
            finally
            {
                worker.ReportProgress(0, getDisplayMember("dropDatabaseUsers{done}", "Dropping user(s) complete."));
            }
        }



        //        private string _defaultMyIniContents = @"
        //# MySQL Server Instance Configuration File
        //# ----------------------------------------------------------------------
        //# Generated by the MySQL Server Instance Configuration Wizard
        //#
        //#
        //# Installation Instructions
        //# ----------------------------------------------------------------------
        //#
        //# On Linux you can copy this file to /etc/my.cnf to set global options,
        //# mysql-data-dir/my.cnf to set server-specific options
        //# (@localstatedir@ for this installation) or to
        //# ~/.my.cnf to set user-specific options.
        //#
        //# On Windows you should keep this file in the installation directory 
        //# of your server (e.g. C:\Program Files\MySQL\MySQL Server X.Y). To
        //# make sure the server reads the config file use the startup option 
        //# ""--defaults-file"". 
        //#
        //# To run run the server from the command line, execute this in a 
        //# command line shell, e.g.
        //# mysqld --defaults-file=""C:\Program Files\MySQL\MySQL Server X.Y\my.ini""
        //#
        //# To install the server as a Windows service manually, execute this in a 
        //# command line shell, e.g.
        //# mysqld --install MySQLXY --defaults-file=""C:\Program Files\MySQL\MySQL Server X.Y\my.ini""
        //#
        //# And then execute this in a command line shell to start the server, e.g.
        //# net start MySQLXY
        //#
        //#
        //# Guildlines for editing this file
        //# ----------------------------------------------------------------------
        //#
        //# In this file, you can use all long options that the program supports.
        //# If you want to know the options a program supports, start the program
        //# with the ""--help"" option.
        //#
        //# More detailed information about the individual options can also be
        //# found in the manual.
        //#
        //#
        //# CLIENT SECTION
        //# ----------------------------------------------------------------------
        //#
        //# The following options will be read by MySQL client applications.
        //# Note that only client applications shipped by MySQL are guaranteed
        //# to read this section. If you want your own MySQL client program to
        //# honor these values, you need to specify it as an option during the
        //# MySQL client library initialization.
        //#
        //[client]
        //
        //port=3306
        //
        //[mysql]
        //
        //default-character-set=utf8
        //
        //
        //# SERVER SECTION
        //# ----------------------------------------------------------------------
        //#
        //# The following options will be read by the MySQL Server. Make sure that
        //# you have installed the server correctly (see above) so it reads this 
        //# file.
        //#
        //[mysqld]
        //
        //# The TCP/IP Port the MySQL Server will listen on
        //port=3306
        //
        //
        //#Path to installation directory. All paths are usually resolved relative to this.
        //#basedir=""C:/Program Files/MySQL/MySQL Server 5.1/""
        //basedir=""__BASEDIR__""
        //
        //
        //#Path to the database root
        //datadir=""__DATADIR__""
        //#datadir=""C:/Documents and Settings/All Users/Application Data/MySQL/MySQL Server 5.1/Data/""
        //
        //# The default character set that will be used when a new schema or table is
        //# created and no character set is defined
        //default-character-set=utf8
        //
        //# The default storage engine that will be used when create new tables when
        //default-storage-engine=INNODB
        //
        //# Set the SQL mode to strict
        //sql-mode=""STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION""
        //
        //# The maximum amount of concurrent sessions the MySQL server will
        //# allow. One of these connections will be reserved for a user with
        //# SUPER privileges to allow the administrator to login even if the
        //# connection limit has been reached.
        //max_connections=100
        //
        //# Query cache is used to cache SELECT results and later return them
        //# without actual executing the same query once again. Having the query
        //# cache enabled may result in significant speed improvements, if your
        //# have a lot of identical queries and rarely changing tables. See the
        //# ""Qcache_lowmem_prunes"" status variable to check if the current value
        //# is high enough for your load.
        //# Note: In case your tables change very often or if your queries are
        //# textually different every time, the query cache may result in a
        //# slowdown instead of a performance improvement.
        //query_cache_size=8M
        //
        //# The number of open tables for all threads. Increasing this value
        //# increases the number of file descriptors that mysqld requires.
        //# Therefore you have to make sure to set the amount of open files
        //# allowed to at least 4096 in the variable ""open-files-limit"" in
        //# section [mysqld_safe]
        //table_cache=256
        //
        //# Maximum size for internal (in-memory) temporary tables. If a table
        //# grows larger than this value, it is automatically converted to disk
        //# based table This limitation is for a single table. There can be many
        //# of them.
        //tmp_table_size=5M
        //
        //
        //# How many threads we should keep in a cache for reuse. When a client
        //# disconnects, the client's threads are put in the cache if there aren't
        //# more than thread_cache_size threads from before.  This greatly reduces
        //# the amount of thread creations needed if you have a lot of new
        //# connections. (Normally this doesn't give a notable performance
        //# improvement if you have a good thread implementation.)
        //thread_cache_size=8
        //
        //#*** MyISAM Specific options
        //
        //# The maximum size of the temporary file MySQL is allowed to use while
        //# recreating the index (during REPAIR, ALTER TABLE or LOAD DATA INFILE.
        //# If the file-size would be bigger than this, the index will be created
        //# through the key cache (which is slower).
        //myisam_max_sort_file_size=100G
        //
        //# If the temporary file used for fast index creation would be bigger
        //# than using the key cache by the amount specified here, then prefer the
        //# key cache method.  This is mainly used to force long character keys in
        //# large tables to use the slower key cache method to create the index.
        //myisam_sort_buffer_size=8M
        //
        //# Size of the Key Buffer, used to cache index blocks for MyISAM tables.
        //# Do not set it larger than 30% of your available memory, as some memory
        //# is also required by the OS to cache rows. Even if you're not using
        //# MyISAM tables, you should still set it to 8-64M as it will also be
        //# used for internal temporary disk tables.
        //key_buffer_size=8M
        //
        //# Size of the buffer used for doing full table scans of MyISAM tables.
        //# Allocated per thread, if a full scan is needed.
        //read_buffer_size=64K
        //read_rnd_buffer_size=185K
        //
        //# This buffer is allocated when MySQL needs to rebuild the index in
        //# REPAIR, OPTIMZE, ALTER table statements as well as in LOAD DATA INFILE
        //# into an empty table. It is allocated per thread so be careful with
        //# large settings.
        //sort_buffer_size=139K
        //
        //
        //#*** INNODB Specific options ***
        //
        //
        //# Use this option if you have a MySQL server with InnoDB support enabled
        //# but you do not plan to use it. This will save memory and disk space
        //# and speed up some things.
        //#skip-innodb
        //
        //# Additional memory pool that is used by InnoDB to store metadata
        //# information.  If InnoDB requires more memory for this purpose it will
        //# start to allocate it from the OS.  As this is fast enough on most
        //# recent operating systems, you normally do not need to change this
        //# value. SHOW INNODB STATUS will display the current amount used.
        //innodb_additional_mem_pool_size=2M
        //
        //# If set to 1, InnoDB will flush (fsync) the transaction logs to the
        //# disk at each commit, which offers full ACID behavior. If you are
        //# willing to compromise this safety, and you are running small
        //# transactions, you may set this to 0 or 2 to reduce disk I/O to the
        //# logs. Value 0 means that the log is only written to the log file and
        //# the log file flushed to disk approximately once per second. Value 2
        //# means the log is written to the log file at each commit, but the log
        //# file is only flushed to disk approximately once per second.
        //innodb_flush_log_at_trx_commit=1
        //
        //# The size of the buffer InnoDB uses for buffering log data. As soon as
        //# it is full, InnoDB will have to flush it to disk. As it is flushed
        //# once per second anyway, it does not make sense to have it very large
        //# (even with long transactions).
        //innodb_log_buffer_size=1M
        //
        //# InnoDB, unlike MyISAM, uses a buffer pool to cache both indexes and
        //# row data. The bigger you set this the less disk I/O is needed to
        //# access data in tables. On a dedicated database server you may set this
        //# parameter up to 80% of the machine physical memory size. Do not set it
        //# too large, though, because competition of the physical memory may
        //# cause paging in the operating system.  Note that on 32bit systems you
        //# might be limited to 2-3.5G of user level memory per process, so do not
        //# set it too high.
        //innodb_buffer_pool_size=8M
        //
        //# Size of each log file in a log group. You should set the combined size
        //# of log files to about 25%-100% of your buffer pool size to avoid
        //# unneeded buffer pool flush activity on log file overwrite. However,
        //# note that a larger logfile size will increase the time needed for the
        //# recovery process.
        //innodb_log_file_size=10M
        //
        //# Number of threads allowed inside the InnoDB kernel. The optimal value
        //# depends highly on the application, hardware as well as the OS
        //# scheduler properties. A too high value may lead to thread thrashing.
        //innodb_thread_concurrency=8
        //
        //# Each table should be in a separate file
        //innodb_file_per_table
        //
        //";

        private string getDisplayMember(string resourceName, string defaultValue, params string[] substitutes) {
            return ResourceHelper.GetDisplayMember(null, "Core", "DatabaseInstaller", resourceName, null, defaultValue, substitutes);
        }

    }
}
