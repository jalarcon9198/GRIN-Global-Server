﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="taxonomysearchcwr.aspx.cs" Inherits="GrinGlobal.Web.taxon.taxonomysearchcwr" MasterPageFile="~/Site1.Master" Title="Crop Wild Relative" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
      
    <asp:Panel ID="pnlSearch" runat="server">
    <br /><center><h2><b>Query Crop Relatives in GRIN-Global</b></h2></center>
<hr />
    Any or all fields can be searched. Wild cards (*) are accepted. Multiple values 
    could be selected from list boxes by using shift or control key.
        <br /><br /><br />
          <center><asp:button id="btnSearch2" style="background-image:url('../images/search.ico'); background-repeat:no-repeat; background-position:2px 2px;" Runat="server" Font-Bold="True" Width="100px" OnClick="btnSearch_Click" Text="Search" BorderColor="#2F571B" BorderStyle="Outset" BorderWidth="2" />      
           <span style="margin-left:25px"></span>
      <asp:Button ID="btnClearAll2" Runat="server" Font-Bold="True" Width="100px" OnClick="btnClearAll_Click" Text="Clear All" BorderColor="#2F571B" BorderStyle="Outset" BorderWidth="2" />      
   <%-- <span style="margin-left:25px"></span>
      <asp:Button ID="Export1" Runat="server" Font-Bold="True" Width="150px" OnClick="Export1_Click" Text="Export to Excel" BorderColor="#2F571B" BorderStyle="Outset" BorderWidth="2" />      
   --%>            </center><br /><br />
        <table>
            <tr>
                <td valign="middle"><b>Crop: </b></td>
        <td><asp:ListBox ID="lstCrop" runat="server" SelectionMode="Multiple" DataTextField="title" DataValueField="value" Rows="10"></asp:ListBox></td>
       
            </tr>
            <tr>
                <td></td>
                 <td><asp:Button ID="btnClearCrop" runat="server" Text="Reset Crops" OnClick="btnClearCrop_Click" /></td>
            </tr>

        </table>
        
            <br />
        <br />
        <b>Genus name: </b>
        <asp:TextBox ID="txtSearch" runat="server" Width="245px"></asp:TextBox>
        (e.g., Oryza [without author])<br />
       <%-- PUT BACK IN  
           <p style="margin-left:80px">
        <asp:CheckBox ID="cbCrop" runat="server" Checked="True" 
            ToolTip="Search for crops" 
            Text="For crops"  />
        &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="cbCWR" runat="server" Checked="True" 
            ToolTip="Search for crop wild relatives" 
            Text="For crop wild relatives" /></p>--%>
        <br />
        <br />
        <b>Genetic relative status:</b>
        &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="cbPrimary" runat="server" Checked="True" 
            ToolTip="Taxa that cross readily with the crop (or can be predicted to do so based on their taxonomic or phylogenetic relationships), yielding (or being expected to yield) fertile hybrids with good chromosome pairing, making gene transfer through hybridization simple." 
            Text="primary" ForeColor="Red" />
        &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="cbSecondary" runat="server" Checked="True" 
            Text="secondary" 
            ToolTip="Taxa that will successfully cross with the crop (or can be predicted to do so based on their taxonomic or phylogenetic relationships), but yield (or would be expected to yield) partially or mostly sterile hybrids with poor chromosome pairing, making gene transfer through hybridization difficult." 
            ForeColor="Red" />
        &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="cbTertiary" runat="server" Checked="True" 
            Text="tertiary" 
            ToolTip="Taxa that can be crossed with the crop (or can be predicted to do so based on their taxonomic or phylogenetic relationships), but hybrids are (or are expected to be) lethal or completely sterile. Special breeding techniques, some yet to be developed, are required for gene transfer." 
            ForeColor="Red" />
        &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="cbGraftstock" runat="server" Checked="True" 
            Text="graftstock" 
            ToolTip="Taxa used as rootstocks for grafting scions of a crop, or used as genetic resources in the breeding of such rootstocks." 
            ForeColor="Red" />
        <br />
        <br />
        <table>
            <tr>
                <td valign="middle"> <b>Family: </b></td>
                <td><asp:ListBox ID="lstFamily" runat="server" SelectionMode="Multiple" 
        DataTextField="family_name" DataValueField="family_name" Rows="10"></asp:ListBox></td>
               
            </tr>
            <tr>
                <td></td>
                 <td> <asp:Button ID="btnClearFamilies" runat="server" Text="Reset Families" OnClick="btnClearFamilies_Click" /></td>
            </tr>
        </table>
<br /><br />  

<b>Native distribution: </b> Continental Area: <asp:DropDownList ID="ddlContinent" DataTextField="continent" DataValueField="continent"
        runat="server" onselectedindexchanged="ddlContinent_SelectedIndexChanged" 
        AutoPostBack="True">
    </asp:DropDownList>   
         &nbsp; &nbsp;
    Region: <asp:DropDownList ID="ddlRegion" DataTextField="subcontinent" DataValueField="region_id" runat="server">
        <asp:ListItem Value="0">ALL REGIONS</asp:ListItem>
    </asp:DropDownList>
  <asp:gridview ID="gvhidden" runat="server" AutoGenerateColumns="true" visible="false"></asp:gridview>
        <br /><br />
        
        <table>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td valign="middle">Country: </td>
                <td><asp:ListBox ID="lstCountry" DataTextField="countryname" 
        DataValueField="countrycode" runat="server" Rows="10" AutoPostBack="True" 
        onselectedindexchanged="lstCountry_SelectedIndexChanged" 
        SelectionMode="Multiple"></asp:ListBox></td>
                <td><asp:label ID="lblState" runat="server" Text="State/Province: " Visible="false"></asp:label><asp:DropDownList ID="ddlState" DataTextField="statename" DataValueField="gid" runat="server" Visible="False">
    </asp:DropDownList></td>
                <td></td>
                
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><asp:Button ID="btnClearCountries" runat="server" Text="Reset Countries" OnClick="btnClearCountries_Click" /></td>
                <td></td>
                <td></td>
            </tr>
        </table>
<br />
        <b><asp:CheckBox ID="cbNonnative" runat="server" Text="Include non-native distribution" /></b><br /> 
 <table>
     <tr>
         <td valign="middle"><b>Restrict to crops maintained at these NPGS repositories: </b>
         </td>
         <td> <asp:ListBox ID="lstRepository" runat="server" SelectionMode="Multiple" DataTextField="title" DataValueField="value" Rows="10"></asp:ListBox></td>
         
     </tr>
     <tr>
         <td></td>
         <td><asp:Button ID="btnClearRepositories" runat="server" Text="Reset Repositories" OnClick="btnClearRepositories_Click" /></td>
     </tr>
 </table>
         <br />
<br />
        <br />
        <asp:RadioButton GroupName="germplasm" ID="rbGerm" runat="server" Text="Restrict to crop wild relatives with germplasm in GRIN" /><br />
        <asp:RadioButton GroupName="germplasm" ID="rbNonGerm" runat="server" Text="Restrict to crop wild relatives without germplasm in GRIN" /><br />
        <asp:RadioButton GroupName="germplasm" ID="rbNone" runat="server" Text="All accessions" checked="true"/><br />
    <%--<asp:CheckBox ID="cbGerm" runat="server" /><b>Restrict to crop wild relatives with germplasm in GRIN</b><br />
    <asp:CheckBox ID="cbNonGerm" runat="server" /><b>Restrict to crop wild relatives without germplasm in GRIN</b> --%>
<br /><br />
  <center><asp:button id="btnSearch" style="background-image:url('../images/search.ico'); background-repeat:no-repeat; background-position:2px 2px;" Runat="server" Font-Bold="True" Width="100px" OnClick="btnSearch_Click" Text="Search" BorderColor="#2F571B" BorderStyle="Outset" BorderWidth="2" />      
            <span style="margin-left:25px"></span>
      <asp:Button ID="btnClearAll" Runat="server" Font-Bold="True" Width="100px" OnClick="btnClearAll_Click" Text="Clear All" BorderColor="#2F571B" BorderStyle="Outset" BorderWidth="2" />      
  <%-- <span style="margin-left:25px"></span>
      <asp:Button ID="btnExport" Runat="server" Font-Bold="True" Width="150px" OnClick="Export1_Click" Text="Export to Excel" BorderColor="#2F571B" BorderStyle="Outset" BorderWidth="2" />      
     --%>
  </center><br /><br />
    </asp:Panel>        
    <asp:Panel ID="pnlResult" runat="server">
<br /><center><h2><b>Crop Relatives in GRIN-Global Taxonomy</b></h2></center>
        <div style="float: right;">
            <asp:Button ID="btnNewCWR" runat="server" Text="New Crop Wild Relatives Search" OnClick="btnClearAll_Click" /></div><br /><br />
        <center><asp:Label ID="lblCriteria" runat="server" Text="" Width="990px"></asp:Label></center> 
        <br />
        Follow links for a) <b>GRIN taxon reports</b> or  b) <b>to view literature supporting this genetic relative classification</b> (Place cursor over underlined items for explanation.) <br /><br />
        <asp:PlaceHolder ID="phData" runat="server"></asp:PlaceHolder>
    </asp:Panel><br />
    <hr />
<asp:Panel id="pnlcite" runat="server" Visible="false">
        <asp:Label ID="lblCite" runat="server"></asp:Label>
    </asp:Panel>
    <br />
</asp:Content>