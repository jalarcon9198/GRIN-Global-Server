DROP PROCEDURE #update_cvl;
GO
CREATE PROCEDURE #update_cvl  
(  
	@group NVARCHAR(100)  
	, @value NVARCHAR(20)
	, @lang NVARCHAR(50)
	, @title NVARCHAR(500)
	, @description NVARCHAR(500)
)  
AS  
BEGIN  
	DECLARE @cvid int;

	IF NOT EXISTS (SELECT 1 FROM code_value WHERE group_name = @group AND value = @value)
		INSERT INTO code_value
			VALUES (@group, @value, GETUTCDATE(), 48, NULL, NULL, GETUTCDATE(), 48);
	   
	SELECT @cvid = code_value_id FROM code_value WHERE group_name = @group AND value = @value

	DECLARE @slid int;
	SELECT @slid = sys_lang_id FROM sys_lang WHERE title = @lang;

	IF EXISTS (SELECT 1 FROM code_value_lang WHERE code_value_id = @cvid AND sys_lang_id = @slid)
		UPDATE code_value_lang
		SET title = @title, description = @description
			,modified_by = 48, modified_date = GETUTCDATE()
		WHERE code_value_id = @cvid AND sys_lang_id = @slid;
	ELSE
		INSERT INTO code_value_lang
		VALUES (@cvid, @slid, @title, @description, GETUTCDATE(), 48, NULL, NULL, GETUTCDATE(), 48);
END
GO


/*
SELECT 
	'EXEC #update_cvl ''' + cv.group_name
	+ ''', ''' + cv.value
	+ ''', ''' + sl.title
	+ ''', ''' + cvl.title
	+ ''', ''' + COALESCE(cvl.description, '')
	+ ''''
FROM code_value cv
INNER JOIN code_value_lang cvl ON cvl.code_value_id = cv.code_value_id
INNER JOIN sys_lang sl ON sl.sys_lang_id = cvl.sys_lang_id
WHERE COALESCE(cvl.modified_date, cvl.created_date) > '2018'
ORDER BY cv.group_name, cvl.description, cvl.sys_lang_id
*/

EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '27', 'English', 'autonomous municipality', '27'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '28', 'English', 'autonomous territorial unit', '28'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '29', 'English', 'dependency', '29'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '30', 'English', 'special city', '30'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '31', 'English', 'district municipality', '31'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '32', 'English', 'chain (of islands)', '32'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '33', 'English', '"islands, groups of islands"', '33'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '34', 'English', 'federal territory', '34'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '35', 'English', 'metropolitan city', '35'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '36', 'English', 'geographical region', '36'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '37', 'English', 'special region', '37'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '38', 'English', 'autonomous community', '38'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '39', 'English', 'country', '39'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '40', 'English', 'town', '40'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '41', 'English', 'autonomous district', '41'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '42', 'English', 'overseas department', '42'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '43', 'English', 'London borough', '43'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '44', 'English', 'popularate', '44'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '45', 'English', 'autonomous province', '45'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '46', 'English', 'republic', '46'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '47', 'English', 'special administrative region', '47'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '48', 'English', 'federal capital territory', '48'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '49', 'English', 'city with county rights', '49'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '50', 'English', 'republican city', '50'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '51', 'English', 'development region', '51'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '52', 'English', 'administrative atoll', '52'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '53', 'English', 'group of islands (20 inhabited islands)', '53'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '54', 'English', 'autonomous city in North Africa', '54'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '55', 'English', 'emirate', '55'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '56', 'English', 'Pakistan administered area', '56'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '57', 'English', 'administrative territory', '57'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '58', 'English', 'ward', '58'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '59', 'English', 'districts under republic administration', '59'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '60', 'English', 'special self-governing city', '60'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '61', 'English', 'autonomous region', '61'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '62', 'English', 'autonomous sector', '62'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '63', 'English', 'administration', '63'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '64', 'English', 'Land', '64'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '65', 'English', 'Union territory', '65'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '66', 'English', 'capital territory', '66'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '67', 'English', 'unitary authority', '67'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '68', 'English', 'federal dependency', '68'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '69', 'English', 'metropolitan district', '69'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '70', 'English', 'arctic region', '70'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '71', 'English', 'capital', '71'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '72', 'English', 'metropolitan administration', '72'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '73', 'English', 'quarter', '73'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '74', 'English', 'autonomous city', '74'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '75', 'English', 'federal district', '75'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '76', 'English', 'council area', '76'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '77', 'English', 'entity', '77'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '78', 'English', 'nation', '78'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '79', 'English', 'special self-governing province', '79'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '80', 'English', 'territorial unit', '80'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '81', 'English', 'commune', '81'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '82', 'English', 'governorate', '82'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '83', 'English', 'geographical entity', '83'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '84', 'English', 'special municipality', '84'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '85', 'English', 'special administrative city', '85'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '86', 'English', 'capital district', '86'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '87', 'English', 'self-governed part', '87'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '88', 'English', 'area', '88'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '89', 'English', 'overseas region', '89'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '90', 'English', 'island council', '90'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '91', 'English', 'parish', '91'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '92', 'English', 'city municipality', '92'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '93', 'English', 'rayon', '93'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '94', 'English', 'geographical unit', '94'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '95', 'English', 'urban community', '95'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '96', 'English', 'metropolitan region', '96'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '97', 'English', 'capital city', '97'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '98', 'English', 'administrative region', '98'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '99', 'English', 'local council', '99'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '100', 'English', 'town council', '100'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '101', 'English', 'indigenous region', '101'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '102', 'English', 'municipality', '102'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '103', 'English', 'special island authority', '103'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '104', 'English', 'city corporation', '104'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '105', 'English', 'canton', '105'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '106', 'English', 'metropolitan department', '106'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '107', 'English', 'outlying area', '107'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '108', 'English', 'administrative precinct', '108'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '109', 'English', 'zone', '109'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '110', 'English', 'island', '110'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '111', 'English', 'overseas territorial collectivity', '111'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '112', 'English', 'economic prefecture', '112'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '113', 'English', 'two-tier county', '113'
EXEC #update_cvl 'GEOGRAPHY_ADMIN1_TYPE', '114', 'English', 'district with special status', '114'
EXEC #update_cvl 'CWR_PRIORITY_SITU', '1', 'English', '1', 'High'
EXEC #update_cvl 'CWR_PRIORITY_SITU', '1', 'ENG', '1', 'High'
EXEC #update_cvl 'CWR_PRIORITY_SITU', '3', 'English', '3', 'Low'
EXEC #update_cvl 'CWR_PRIORITY_SITU', '3', 'ENG', '3', 'Low'
EXEC #update_cvl 'CWR_PRIORITY_SITU', '2', 'English', '2', 'Medium'
EXEC #update_cvl 'CWR_PRIORITY_SITU', '2', 'ENG', '2', 'Medium'
EXEC #update_cvl 'CWR_PRIORITY_SITU', '4', 'English', '4', 'None'
EXEC #update_cvl 'CWR_PRIORITY_SITU', '4', 'ENG', '4', 'None'
EXEC #update_cvl 'CWR_PRIORITY_STATUS', '1A', 'English', '1A', 'Close relative of important crop'
EXEC #update_cvl 'CWR_PRIORITY_STATUS', '1A', 'ENG', '1A', 'Close relative of important crop'
EXEC #update_cvl 'CWR_PRIORITY_STATUS', '1B', 'English', '1B', 'Distant relative of important crop'
EXEC #update_cvl 'CWR_PRIORITY_STATUS', '1B', 'ENG', '1B', 'Distant relative of important crop'
EXEC #update_cvl 'CWR_PRIORITY_STATUS', '2', 'English', '2', 'Relative of other crops'
EXEC #update_cvl 'CWR_PRIORITY_STATUS', '2', 'ENG', '2', 'Relative of other crops'