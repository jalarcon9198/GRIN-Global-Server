/*
   Schema change to add method_id column to accession_inv_group table and index it
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

BEGIN TRANSACTION
GO

DECLARE @drop_sql NVARCHAR(MAX) = N'';
DECLARE @table_name nvarchar(255) = 'accession_inv_group';

-- drop default constraints on table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name)
	+ ' DROP CONSTRAINT ' + QUOTENAME(dc.name) + '; '
FROM sys.default_constraints AS dc
INNER JOIN sys.tables AS ct ON dc.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE dc.parent_object_id = OBJECT_ID(@table_name)

-- drop foreign key constraints involving the table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
    + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + '; '
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS ct ON fk.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE fk.parent_object_id = OBJECT_ID(@table_name) OR fk.referenced_object_id = OBJECT_ID(@table_name)

-- execute the drops
EXECUTE sp_executesql @drop_sql
COMMIT

BEGIN TRANSACTION
GO
ALTER TABLE dbo.cooperator SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_accession_inv_group
	(
	accession_inv_group_id int NOT NULL IDENTITY (1, 1),
	group_name nvarchar(100) NOT NULL,
	method_id int NULL,
	is_web_visible nvarchar(1) NOT NULL,
	note nvarchar(MAX) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_accession_inv_group SET (LOCK_ESCALATION = TABLE)
GO
GRANT SELECT ON dbo.Tmp_accession_inv_group TO gg_search  AS dbo
GO
ALTER TABLE dbo.Tmp_accession_inv_group ADD CONSTRAINT
	DF__accession__is_we__09DE7BCC DEFAULT ('N') FOR is_web_visible
GO
SET IDENTITY_INSERT dbo.Tmp_accession_inv_group ON
GO
IF EXISTS(SELECT * FROM dbo.accession_inv_group)
	 EXEC('INSERT INTO dbo.Tmp_accession_inv_group (accession_inv_group_id, group_name, is_web_visible, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT accession_inv_group_id, group_name, is_web_visible, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.accession_inv_group WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_accession_inv_group OFF
GO
DROP TABLE dbo.accession_inv_group
GO
EXECUTE sp_rename N'dbo.Tmp_accession_inv_group', N'accession_inv_group', 'OBJECT' 
GO
ALTER TABLE dbo.accession_inv_group ADD CONSTRAINT
	PK_accession_inv_group PRIMARY KEY CLUSTERED 
	(
	accession_inv_group_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX ndx_fk_aig_created ON dbo.accession_inv_group
	(
	created_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_aig_modified ON dbo.accession_inv_group
	(
	modified_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_aig_owned ON dbo.accession_inv_group
	(
	owned_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_aig ON dbo.accession_inv_group
	(
	group_name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

-- add the method FK and index
CREATE NONCLUSTERED INDEX [ndx_fk_aig_m] ON [dbo].[accession_inv_group]
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[accession_inv_group]  WITH CHECK ADD  CONSTRAINT [fk_aig_m] FOREIGN KEY([method_id])
REFERENCES [dbo].[method] ([method_id])
GO

ALTER TABLE [dbo].[accession_inv_group] CHECK CONSTRAINT [fk_aig_m]
GO

----

ALTER TABLE dbo.accession_inv_group ADD CONSTRAINT
	fk_aig_created FOREIGN KEY
	(
	created_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_group ADD CONSTRAINT
	fk_aig_modified FOREIGN KEY
	(
	modified_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_group ADD CONSTRAINT
	fk_aig_owned FOREIGN KEY
	(
	owned_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_inv_group', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_inv_group', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_inv_group', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_inv_group_attach ADD CONSTRAINT
	fk_aigat_aig FOREIGN KEY
	(
	accession_inv_group_id
	) REFERENCES dbo.accession_inv_group
	(
	accession_inv_group_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_group_attach SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_inv_group_attach', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_inv_group_attach', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_inv_group_attach', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_inv_group_map ADD CONSTRAINT
	fk_aigm_aig FOREIGN KEY
	(
	accession_inv_group_id
	) REFERENCES dbo.accession_inv_group
	(
	accession_inv_group_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_group_map SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_inv_group_map', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_inv_group_map', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_inv_group_map', 'Object', 'CONTROL') as Contr_Per 